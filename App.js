import React from 'react';
import { Provider } from 'react-redux'
import { createStore, combineReducers, applyMiddleware } from 'redux'
import ReduxThunk from 'redux-thunk'
import Reducer from './src/reducers/reducers';

import MainStack from './src/navigators/mainStack';
require('moment/locale/pt-br.js');


const Reducers = combineReducers({
  Reducer: Reducer
})


const store = createStore(Reducers, applyMiddleware(ReduxThunk))

export default function App() {
  return (
    <Provider store={store}>
      <MainStack />
    </Provider>
  )
}