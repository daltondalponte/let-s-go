import { StyleSheet } from 'react-native';

export const style = StyleSheet.create({
	container: {
		flex: 1,
		justifyContent: 'center',
		marginLeft: '10%',
		marginRight: '10%',
	},
	rowAlign: {
		flexDirection: 'row',
		alignItems: 'center',
		marginBottom: 20
	},
	headerText: {
		fontSize: 30,
		color: '#696969',
		fontFamily: 'MuseoSans-100',
	},
	normalText: {
		fontSize: 16,
		color: '#696969',
		fontFamily: 'MuseoSans-300',
	},
	titleText: {
		fontSize: 22,
		color: '#696969',
		fontFamily: 'MuseoSans-300',
	},
	normalTextBold: {
		fontSize: 16,
		color: '#696969',
		fontFamily: 'MuseoSans-500',
	},
	titleTextBold: {
		fontSize: 22,
		color: '#696969',
		fontFamily: 'MuseoSans-500',
	},
	roundOrangeBorder: {
		height: 50,
		borderColor: '#ff7f00',
		borderWidth: 1,
		borderRadius: 25,
	},
	inputText: {
		fontFamily: 'MuseoSans-300',
		paddingLeft: 20,
		height: 50,
		borderColor: '#ff7f00',
		borderWidth: 1,
		borderRadius: 25,
		marginBottom: 7
	},
	inputTextMultiLine: {
		fontFamily: 'MuseoSans-300',
		paddingLeft: 40,
		flexDirection: 'row',
		height: 100,
		alignItems: 'center',
		borderColor: '#ff7f00',
		borderWidth: 1,
		borderRadius: 50,
		marginBottom: 7
	},
	photo: {
		height: '90%',
		width: '90%',
		borderRadius: 100,
		borderWidth: 1,
		borderColor: '#ff7f00',
		alignItems: 'center',
		justifyContent: 'center',
	},
	photoBig: {
		height: 150,
		width: 150,
		borderRadius: 75,
		borderWidth: 1,
		borderColor: '#ff7f00',
		alignItems: 'center',
		justifyContent: 'center',
	},
	photoEvent: {
		width: 400,
		height: 250,
		marginTop: 5,
		borderRadius: 40,
		borderWidth: 1,
		borderColor: '#ff7f00',
		alignSelf: 'center',
	},
	dateBox: {
        marginTop: 10,
        flexDirection: 'row',
        alignItems: 'center',
        width: '100%',
        height: 40,
        backgroundColor: '#eeeeee',
	},
	dateBoxDetail: {
        marginTop: 10,
        justifyContent: 'center',
        width: '100%',
		height: 60,
        backgroundColor: '#eeeeee',
	},
	dateBoxDetailWithButtons: {
        marginTop: 10,
        //justifyContent: 'center',
        width: '100%',
		height: 60,
        backgroundColor: '#eeeeee',
	},
	absoluteLeft: {
		position: 'absolute',
		bottom: 20,
		left: 20
	},
	absoluteRight: {
		position: 'absolute',
		bottom: 20,
		right: 20
	}
});
