import React, { Component } from 'react'
import { TouchableOpacity, StyleSheet, Text, View } from 'react-native'
import Icon from "react-native-vector-icons/FontAwesome";
import LinearGradient from 'react-native-linear-gradient';

export default Header = (props) => {
  return (
    <LinearGradient colors={['rgba(255, 127, 0, 1)', 'rgba(238, 191, 36, 1)']} style={styles.header}>
      <View style={styles.container}>
        <TouchableOpacity onPress={props.onPress}>
          <Icon name="bars" size={30} style={{ color: "#fff" }} />
        </TouchableOpacity>
        <Text style={styles.textBold}>{props.text}</Text>
        <Text style={styles.titleTextBold}>{props.title}</Text>

      </View>
    </LinearGradient>
  )
}


const styles = StyleSheet.create({
  header: {
    height: 70,
    backgroundColor: '#ff7f00',
  },
  container: {
    flexDirection: 'row',
    flex: 1,
    alignItems: 'center',
    marginLeft: 20
  },
	titleTextBold: {
    position: 'absolute',
    right: 20,
    top: 30,
    fontSize: 20,
		color: '#fff',
		fontFamily: 'MuseoSans-300',
  },
	textBold: {
    position: 'absolute',
    right: 20,
    bottom: 40,
		fontSize: 15,
		color: '#fff',
		fontFamily: 'MuseoSans-100',
	},
})

