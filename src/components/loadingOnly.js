import React, { Component } from 'react';
import { StyleSheet, View, ActivityIndicator } from 'react-native';

const LoadingOnly = (props) => {

    return (

        <ActivityIndicator
            style={{
                position: props.position,
                left: props.left,
                right: props.right,
                top: props.top,
                bottom: props.bottom,
                justifyContent: props.justifyContent,
                alignItems: props.alignItems
            }}
            animating={props.loading}
            size={props.size}
            color="#rgba(238, 191, 36, 1)"
        />

    )
}


export default LoadingOnly;