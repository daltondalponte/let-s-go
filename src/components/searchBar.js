import React, { useState, useEffect } from 'react'
import { SearchBar } from 'react-native-elements'
import { Text, View, Platform, TextInput, Modal, FlatList } from 'react-native'
import { style } from '../styles'
import { useSelector, useDispatch } from 'react-redux'
import { getAllEvents } from '../functions/authFunctions'

const SearchInput = (props) => {
    const dispatch = useDispatch()
    const [search, setSearch] = useState('')
    const [searchModal, setSearchModal] = useState(true)


    useEffect(() => {
        getAllEvents(dispatch)
    }, [])

    const allEvents = useSelector(state => state.Reducer.allEvents)
    const userInfo = useSelector(state => state.Reducer.userInfo)

    const event = allEvents.filter(event => {
        return event.name.toLowerCase().includes(search.toLowerCase())
    })

    return (
        <View>
            {search.length > 1 &&
                <View style={{ marginBottom: 50, }}>
                    <Text style={[style.titleText, { padding: 10, borderColor: '#bbbbbb', borderBottomWidth: 1 }]}>Resultados:</Text>
                    <FlatList
                        data={event}
                        renderItem={({ item }) => <View>
                            <View style={{ borderColor: '#bbbbbb', borderBottomWidth: 1 }}>
                                <View style={style.dateBox}>
                                    <Text onPress={() => { console.log(item) }} style={[{ marginLeft: 20 }, style.titleTextBold]}>{item.name}</Text>
                                </View>
                                <Text style={[{ marginLeft: 20 }, style.normalTextBold]}>{item.dateDescription}</Text>
                                <Text style={[style.normalText, { marginLeft: 20, marginBottom: 20 }]}>{item.description}</Text>
                            </View>
                        </View>
                        }
                        keyExtractor={(item) => item.id}
                        showsVerticalScrollIndicator={false}
                    />
                </View>}



            <SearchBar
                placeholder="Buscar..."
                onChangeText={setSearch}
                value={search}
                inputContainerStyle={{
                    opacity: 0.9,
                    borderRadius: 20,
                    shadowColor: '#000000',
                    backgroundColor: '#FFF',
                    shadowOpacity: 0.1,
                    shadowRadius: 15,
                    shadowOffset: { x: 0, y: 0, },
                    elevation: 5,
                }}
                inputStyle={{
                    fontFamily: 'MuseoSans-300'
                }}
                containerStyle={{
                    position: 'absolute',
                    bottom: 20,
                    alignSelf: 'center',
                    height: 35,
                    width: '95%',
                    backgroundColor: 'transparent',
                    borderBottomColor: 'transparent',
                    borderTopColor: 'transparent',
                }}
            />
        </View>
    );
}

export default SearchInput