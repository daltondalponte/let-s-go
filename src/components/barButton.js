import React from 'react';
import { Text, StyleSheet, TouchableOpacity, View } from 'react-native';
import LinearGradient from 'react-native-linear-gradient';


export default (props) => {

  return (
    <TouchableOpacity
      style={{
        position: props.position,
        top: props.top,
        bottom: props.bottom,
        left: props.left,
        right: props.right,
        opacity: props.opacity,
        marginTop: props.marginTop,
        marginBottom: props.marginBottom,
        marginLeft: props.marginLeft,
        marginRight: props.marginRight,
        alignSelf: props.alignSelf,
        width: props.width,
        height: 50,
        alignItems: 'center',
        justifyContent: 'center',
        padding: 10,
        borderRadius: 25,
      }}
      onPress={props.onPress}>
      <LinearGradient colors={['rgba(255, 127, 0, 1)', 'rgba(238, 191, 36, 1)']}
        style={{
          opacity: props.opacity,
          width: props.width,
          height: 50,
          alignItems: 'center',
          justifyContent: 'center',
          padding: 10,
          borderRadius: 25,
        }}>
        <Text style={style.buttonText}>{props.label}</Text>
      </LinearGradient>
    </TouchableOpacity >
  );
}


const style = StyleSheet.create({
  buttonText: {
    fontSize: 18,
    color: '#fff',
    fontFamily: 'MuseoSans-300',
  },
})