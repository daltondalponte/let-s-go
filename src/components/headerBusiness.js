import React, { Component } from 'react'
import { TouchableOpacity, StyleSheet, Text } from 'react-native'
import Icon from "react-native-vector-icons/FontAwesome"
import LinearGradient from 'react-native-linear-gradient'
import { style } from '../styles'


export default HeaderBusiness = (props) => {
  return (
    <LinearGradient
      colors={['rgba(255, 127, 0, 1)', 'rgba(238, 191, 36, 1)']}
      style={styles.header}>
      <TouchableOpacity onPress={props.onPress}>
        <Icon name="chevron-left" size={25} style={{ color: "#fff", marginLeft: 15 }} />
      </TouchableOpacity>
      <Text style={[style.headerText, { color: '#fff', marginLeft: 20 }]}> {props.label}</Text>
    </LinearGradient>
  )
}


const styles = StyleSheet.create({
  header: {
    alignItems: 'center',
    flexDirection: 'row',
    height: 70,
    backgroundColor: '#ff7f00',

  }
})

