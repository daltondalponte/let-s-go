import React, { useState, useEffect } from 'react';
import { Text, View } from 'react-native'
import { GooglePlacesAutocomplete } from 'react-native-google-places-autocomplete'
import { useSelector, useDispatch } from 'react-redux'


const AddressAutocomplete = (props) => {
  const dispatch = useDispatch()


  return (
    <GooglePlacesAutocomplete
      placeholder='Digite seu endereço e confirme'
      placeholderTextColor='#B4B4B4'
      onPress={(data, details) => {
        dispatch({ type: 'SET_TMP_ADDRESS', tmp_address: details })
      }}
      query={{
        key: 'AIzaSyBzwQrdFLxsNTvNO_TtX2jws76dPPk5dMM',
        language: 'pt'
      }}
      autoFocus={true}
      textInputProps={{
        autoCapitalize: 'none',
        autoCorrect: false
      }}
      listViewDisplayed='auto'
      fetchDetails={true}
      enablePoweredByContainer={false}
      styles={{
        container: {
          
        },
        textInputContainer: {
          borderTopWidth: 0,
          borderBottomWidth: 1,
          backgroundColor: 'transparent',
        },
        textInput: {
          fontFamily: 'MuseoSans-300',
        },
        listView: {
          marginVertical: 10,
          marginHorizontal: 10

        },
        description: {},
        row: {}
      }}
    />
  )
}

export default AddressAutocomplete
