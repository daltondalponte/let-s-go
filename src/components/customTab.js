import React, { Component } from 'react'
import { TouchableHighlight, StyleSheet, View, Text } from 'react-native'
import Icon from "react-native-vector-icons/FontAwesome";
import { style } from '../styles'
import LinearGradient from 'react-native-linear-gradient';



const CustomTab = (props) => {
    return (
        <View style={styles.tabArea}>
            {props.items.map(item => (
                <View key={item.route} style={styles.tabItem}>
                    {item.type == 'profile' &&
                        <TouchableHighlight style={{ alignItems: 'center' }} underlayColor='transparent' onPress={() => { props.navigation.navigate(item.route) }}>
                            <>
                                <Icon name="user" size={20} style={{ color: '#fff' }} />
                                <Text style={[style.normalText, { color: '#fff' }]}>{item.text}</Text>
                            </>
                        </TouchableHighlight>
                    }
                    {item.type == 'calendar' &&
                        <TouchableHighlight style={{ alignItems: 'center' }} underlayColor='transparent' onPress={() => { props.navigation.navigate(item.route) }}>
                            <>
                                <View style={styles.tabBall}>
                                    <LinearGradient style={styles.tabBall2}
                                        colors={['rgba(255, 127, 0, 1)', 'rgba(238, 191, 36, 1)']}>

                                        <Icon name="calendar" size={30} style={{ color: '#fff' }} />
                                        <Text style={[style.normalText, { color: '#fff' }]}>{item.text}</Text>
                                    </LinearGradient>
                                </View>
                            </>
                        </TouchableHighlight>
                    }
                    {item.type == 'about' &&
                        <TouchableHighlight style={{ alignItems: 'center' }} underlayColor='transparent' onPress={() => { props.navigation.navigate(item.route) }}>
                            <>
                                <Icon name="info" size={20} style={{ color: '#fff' }} />
                                <Text style={[style.normalText, { color: '#fff' }]}>{item.text}</Text>
                            </>
                        </TouchableHighlight>
                    }

                </View>
            ))}
        </View>
    )
}


const styles = StyleSheet.create({
    tabArea: {
        flexDirection: 'row',
        backgroundColor: '#b4b4b4',
    },
    tabItem: {
        flex: 1,
        height: 70,
        justifyContent: 'center'

    },
    tabBall: {
        height: 110,
        width: 110,
        borderRadius: 55,
        backgroundColor: '#fff',
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: -20,
    },
    tabBall2: {
        height: 100,
        width: 100,
        borderRadius: 50,
        justifyContent: 'center',
        alignItems: 'center'
    }

})

export default CustomTab

