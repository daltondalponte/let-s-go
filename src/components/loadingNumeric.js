import React, { Component } from 'react';
import { StyleSheet, View, Modal, ActivityIndicator, Text } from 'react-native';

const LoadingNumeric = (props) => {

    return (
        <Modal
            transparent={true}
            animationType={'none'}
            visible={props.loading}
            onRequestClose={() => { console.log('close modal') }}>
            <View style={styles.modalBackground}>
                <View style={styles.activityIndicatorWrapper}>
                    <ActivityIndicator
                        animating={props.loading}
                        size="large"
                        color="#ffffff"
                    />
                </View>
                <Text style={styles.titleText}>{props.percentage}%</Text>
            </View>
        </Modal>
    )
}

const styles = StyleSheet.create({
    modalBackground: {
        flex: 1,
        alignItems: 'center',
        flexDirection: 'column',
        justifyContent: 'center',
        backgroundColor: '#00000040'
    },
    activityIndicatorWrapper: {
        backgroundColor: 'rgba(255,255,255,0.4)',
        height: 40,
        width: 40,
        borderRadius: 20,
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'space-around'
    },
	titleText: {
		fontSize: 30,
		color: '#fff',
        fontFamily: 'MuseoSans-300',
	},
});

export default LoadingNumeric