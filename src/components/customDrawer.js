
import React, { useState, useEffect } from 'react';
import { Text, View, StyleSheet, TouchableHighlight } from 'react-native'
import Icon from 'react-native-vector-icons/MaterialCommunityIcons'
import { style } from '../styles'
import { useSelector, useDispatch } from 'react-redux'
import { getUserInfo, handleLogOut, getPlaceData } from '../functions/authFunctions'


const CustomDrawer = (props) => {
  const dispatch = useDispatch()

  const userInfo = useSelector(state => state.Reducer.userInfo)



  const [triggerModal, setTriggerModal] = useState(false)
  const [eventForManage, setEventForManage] = useState(true)


  useEffect(() => {
    getUserInfo(dispatch)
  }, []);


  return (
    <View style={{ flex: 1, padding: 20, backgroundColor: 'rgba(255,255,255,0.8)' }}>
      <View style={{ borderBottomWidth: 1, borderColor: '#696969', marginBottom: 30 }}>
        <Text style={[{ marginBottom: 20 }, style.titleText]}>Bem Vindo, {userInfo.name}</Text>
      </View>
      <TouchableHighlight style={style.rowAlign} underlayColor='transparent' onPress={() => props.navigation.navigate('Home')}>
        <>
          <Icon name="home-circle" size={30} style={{ marginRight: 10, color: '#696969' }} />
          <Text style={style.normalText}>Home</Text>
        </>
      </TouchableHighlight>
      <TouchableHighlight style={style.rowAlign} underlayColor='transparent' onPress={() => props.navigation.navigate('Profile')}>
        <>
          <Icon name="account-circle" size={30} style={{ marginRight: 10, color: '#696969' }} />
          <Text style={style.normalText}>Perfil</Text>
        </>
      </TouchableHighlight>
      {userInfo.userType === 'Professional' &&
        <TouchableHighlight style={{ marginTop: 60, borderBottomWidth: 1, borderTopWidth: 1, borderColor: '#cccccc' }} underlayColor='transparent' onPress={() => { getPlaceData(userInfo.uid, triggerModal, eventForManage, dispatch), props.navigation.navigate('CreateEvent') }}>
          <View style={[style.rowAlign, { marginTop: 20 }]}>
            <Icon name="plus" size={30} style={{ marginRight: 10, color: '#696969' }} />
            <Text style={style.normalText}>Criar Evento</Text>
          </View>
        </TouchableHighlight>
      }
      {userInfo.userType === 'Professional' &&
        <TouchableHighlight style={{ marginTop: 0, borderBottomWidth: 1, borderColor: '#cccccc' }} underlayColor='transparent' onPress={() => {getPlaceData(userInfo.uid, triggerModal, eventForManage, dispatch), props.navigation.navigate('Manage')}}>
          <View style={[style.rowAlign, { marginTop: 20 }]}>
            <Icon name="circle-edit-outline" size={30} style={{ marginRight: 10, color: '#696969' }} />
            <Text style={style.normalText}>Gerenciar Eventos</Text>
          </View>
        </TouchableHighlight>
      }
      {userInfo.userType === 'Professional' &&
        <TouchableHighlight style={{ marginTop: 0, borderBottomWidth: 1, borderColor: '#cccccc' }} underlayColor='transparent' onPress={() => props.navigation.navigate('AdministrativeProfiles')}>
          <View style={[style.rowAlign, { marginTop: 20 }]}>
            <Icon name="account-supervisor-circle" size={30} style={{ marginRight: 10, color: '#696969' }} />
            <Text style={style.normalText}>Perfis Administrativos</Text>
          </View>
        </TouchableHighlight>
      }



      <TouchableHighlight underlayColor='transparent' onPress={() => { handleLogOut(props.navigation, dispatch) }} style={[{ position: 'absolute', bottom: 0, right: 0, marginRight: 10 }, style.rowAlign]}>
        <>
          <Text style={style.titleText}>Sair</Text>
          <Icon name="logout-variant" size={20} style={{ marginRight: 10, marginLeft: 5, color: '#696969' }} />
        </>
      </TouchableHighlight>
    </View>
  );
}



export default CustomDrawer;