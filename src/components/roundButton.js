import React from 'react';
import { Text, StyleSheet, TouchableOpacity, View } from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons'



export default (props) => {

    return (
      <TouchableOpacity style={{
        alignSelf: 'flex-end',
        position: 'absolute',
        bottom: 25,
        right: 25
        }}
        onPress={props.onPress}>
        <LinearGradient colors={['rgba(255, 127, 0, 1)', 'rgba(238, 191, 36, 1)']}
          style={{
            opacity: props.opacity,
            marginTop: props.marginTop,
            marginBottom: props.marginBottom,
            marginLeft: props.marginLeft,
            marginRight: props.marginRight,
            width: 80,
            height: 80,
            alignItems: 'center',
            justifyContent: 'center',
            borderRadius: 50,
          }}>
          <Icon name='check' size={50} color='white'/>
        </LinearGradient>
      </TouchableOpacity>
    );
  }


const style = StyleSheet.create({
  buttonText: {
    fontSize: 18,
    color: '#fff',
    fontFamily: 'MuseoSans-300',
  },
})