import { createStackNavigator } from 'react-navigation-stack'

import CreateEvent from '../screens/businessTabs/createEvent'
import EventPage from '../screens/businessTabs/eventPage'

const BusinessStack = createStackNavigator({
    CreateEvent: CreateEvent,
    EventPage: EventPage
}, {
    initialRouteName: 'CreateEvent',
    defaultNavigationOptions: {
        headerShown: false
    }
})

export default BusinessStack