import React, { Component } from 'react'

import { createAppContainer } from 'react-navigation'
import { createStackNavigator } from 'react-navigation-stack'
import { createDrawerNavigator } from 'react-navigation-drawer'
import { createBottomTabNavigator } from 'react-navigation-tabs'


import Preload from '../screens/preload'
import SignIn from '../screens/authentication/signIn'
import SignUp from '../screens/authentication/signUp'
import SignUpAdministrative from '../screens/authentication/signUpAdministrative'

import SignUpBusiness from '../screens/authentication/signUpBusiness'
import ForgotPassword from '../screens/authentication/forgotPassword'

import Home from '../screens/home'
import Profile from '../screens/drawer/profile'
import EditProfile from '../screens/drawer/editProfile'
import Manage from '../screens/drawer/manage'
import EditEvent from '../screens/drawer/editEvent'
import ListManage from '../screens/drawer/listManage'
import AdministrativeProfiles from '../screens/drawer/administrativeProfiles'

import HomeAdministrative from '../screens/homeAdministrative'

import CustomDrawer from '../components/customDrawer'

import ProfilePage from '../screens/businessTabs/profilePage'
import Calendar from '../screens/businessTabs/calendar'
import About from '../screens/businessTabs/about'
import CustomTab from '../components/customTab'

import BusinessStack from '../navigators/businessStack'

const AuthStack = createStackNavigator({
    SignIn: SignIn,
    SignUp: SignUp,
    SignUpBusiness: SignUpBusiness,
    ForgotPassword: ForgotPassword
}, {
    initialRouteName: 'SignIn',
    defaultNavigationOptions: {
        headerShown: false
    }
})


const HomeDrawer = createDrawerNavigator({
    Home: Home,
    Profile: Profile,
    EditProfile: EditProfile,
    Manage: Manage,
    EditEvent: EditEvent,
    ListManage: ListManage,
    AdministrativeProfiles: AdministrativeProfiles,
    SignUpAdministrative: SignUpAdministrative,
    HomeAdministrative: HomeAdministrative,

}, {
    initialRouteName: 'Home',
    drawerBackgroundColor: "transparent ",
    contentComponent: CustomDrawer
})

const ProfileTab = createBottomTabNavigator({
    ProfilePage,
    Calendar,
    About,
    BusinessStack: {
        screen: BusinessStack,
        navigationOptions: () => {
            return {
                tabBarVisible: false,
            }
        }
    }
}, {
    tabBarComponent: (props) => (
        <CustomTab {...props}
            items={[
                {
                    type: 'profile',
                    text: 'Perfil',
                    icon: 'icon',
                    route: 'ProfilePage'
                },
                {
                    type: 'calendar',
                    text: 'Agenda',
                    icon: 'icon',
                    route: 'Calendar'
                },
                {
                    type: 'about',
                    text: 'Sobre',
                    icon: 'icon',
                    route: 'About'
                },
            ]}
        />
    )
})

const MainStack = createStackNavigator({
    Preload,
    AuthStack,
    HomeDrawer,
    ProfileTab,
}, {
    initialRouteName: 'Preload',
    defaultNavigationOptions: {
        headerShown: false
    }
})



export default createAppContainer(MainStack)