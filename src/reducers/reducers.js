
const initialState = {
    loginStatus: '',
    tmp_address: '',
    userInfo: '',
    markers: [],
    placeInfo: '',
    placeEvents: [],
    placeEventsForManage: [],
    allEvents: [],
    administrativeProfiles: [],
    photoProgress: {
        loading: false,
        progress: 0,
        downloadURL: ''
    },
    imagePickerResponse: '',
    homeModal: false,
    loadingAuth: false,
    loadingEvent: false,
};


function Reducer(state = initialState, action) {
    switch (action.type) {
        case 'SET_STATUS':
            return { ...state, loginStatus: action.loginStatus }
        case 'SET_HOME_MODAL':
            return { ...state, homeModal: action.homeModal }
        case 'SET_LOADING_AUTH':
            return { ...state, loadingAuth: action.loadingAuth }
        case 'SET_LOADING_EVENT':
            return { ...state, loadingEvent: action.loadingEvent }
        case 'SET_TMP_ADDRESS':
            return { ...state, tmp_address: action.tmp_address }
        case 'GET_USERINFO':
            return { ...state, userInfo: action.userInfo }
        case 'GET_MARKERS':
            return { ...state, markers: action.markers }
        case 'GET_PLACEINFO':
            return { ...state, placeInfo: action.placeInfo }
        case 'GET_PLACEEVENTS':
            return { ...state, placeEvents: action.placeEvents }
        case 'GET_ALL_EVENTS':
            return { ...state, allEvents: action.allEvents }
        case 'GET_PLACEEVENTS_FOR_MANAGE':
            return { ...state, placeEventsForManage: action.placeEventsForManage }
        case 'ADMINISTRATIVE_PROFILES':
            return { ...state, administrativeProfiles: action.administrativeProfiles }
        case 'PHOTO_UPLOAD_PROGRESS':
            return { ...state, photoProgress: action.photoProgress }
        case 'IMAGEPICKER_RESPONSE':
            return { ...state, imagePickerResponse: action.imagePickerResponse }
        default: return state

    }
}

export default Reducer;