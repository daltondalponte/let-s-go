
import firebase from '../services/firebase'
import moment from 'moment'
import { StackActions, NavigationActions } from 'react-navigation'


export const preloadCheck = async (dispatch, navigation) => {
    try {
        await firebase.auth().onAuthStateChanged(async (user) => {
            if (user) {
                await firebase.firestore().collection("users").doc(user.uid).get().then(function (doc) {
                    const userType = doc.data().userType
                    if (userType == 'Administrative') {
                        dispatch({ type: 'SET_STATUS', loginStatus: 1 })
                        navigation.navigate('HomeAdministrative')
                    } else if (userType == 'Personal') {
                        dispatch({ type: 'SET_STATUS', loginStatus: 2 })
                        navigation.dispatch(StackActions.reset({
                            index: 0, actions: [NavigationActions.navigate({ routeName: 'HomeDrawer' })]
                        }))
                    } else if (userType == 'Professional') {
                        dispatch({ type: 'SET_STATUS', loginStatus: 2 })
                        navigation.dispatch(StackActions.reset({
                            index: 0, actions: [NavigationActions.navigate({ routeName: 'HomeDrawer' })]
                        }))
                    } else {
                        console.log('não achou tipo de usuario')
                    }
                })
            } else if (user == null) {
                dispatch({ type: 'SET_STATUS', loginStatus: 0 })
                navigation.dispatch(StackActions.reset({
                    index: 0, actions: [NavigationActions.navigate({ routeName: 'AuthStack' })]
                }))
            }
        })
        getMarkers(dispatch)
    } catch (error) {
        console.log(error)
    }
}








// GET INFORMATIONS - USER, EVENTS, MAP MARKERS

export const getUserInfo = async (dispatch) => {
    try {
        await firebase.firestore().collection("users").doc(firebase.auth().currentUser.uid).get().then(function (doc) {
            if (doc.exists) {
                dispatch({
                    type: 'GET_USERINFO', userInfo: {
                        name: doc.data().name,
                        email: doc.data().email,
                        address: doc.data().address,
                        cnpj: doc.data().cnpj,
                        cpf: doc.data().cpf,
                        employer: doc.data().employer,
                        description: doc.data().description,
                        photos: doc.data().photos,
                        uid: doc.id,
                        userType: doc.data().userType
                    }
                })
                if (doc.data().userType === 'Professional') {
                    const triggerModal = false
                    const eventForManage = false
                    getPlaceEvents(doc.id, triggerModal, eventForManage, dispatch)
                    getAdministrativeProfiles(doc.id, dispatch)
                } else if (doc.data().userType === 'Administrative') {
                    const triggerModal = false
                    const eventForManage = true
                    getPlaceEvents(doc.data().employer, triggerModal, eventForManage, dispatch)
                }
            }
        })
    } catch (error) {
        console.log(error)
    }
}


export const getPlaceData = async (uid, triggerModal, eventForManage, dispatch) => {
    dispatch({ type: 'SET_LOADING_EVENT', loadingEvent: true })
    try {
        await firebase.firestore().collection("users").doc(uid).get().then(function (doc) {
            if (doc.exists) {
                dispatch({
                    type: 'GET_PLACEINFO', placeInfo: {
                        name: doc.data().name,
                        email: doc.data().email,
                        address: doc.data().address,
                        description: doc.data().description,
                        photos: doc.data().photos,
                        uid: doc.id
                    }
                })
                getPlaceEvents(uid, triggerModal, eventForManage, dispatch)
            }
        })
    } catch (error) {
        console.log(error)
    }
}

export const getAllEvents = async (dispatch) => {
    try {
        let events = []
        await firebase.firestore().collection("events")
            .get().then((snapshot) => {
                snapshot.forEach(async (doc) => {
                    events.push({
                        dateDescription: doc.data().dateDescription,
                        name: doc.data().name,
                        photos: doc.data().photos,
                        enableListNames: doc.data().enableListNames,
                        listNames: doc.data().listNames,
                        description: doc.data().description,
                        dateTimestamp: doc.data().dateTimestamp,
                        administrators: doc.data().administrators,
                        id: doc.id,
                        owner: doc.data().owner,
                        list: doc.data().list
                    })
                })
                moment.locale('pt-br')
                const eventsNowOn = events.filter((x) => new moment(x.dateTimestamp).format('YYYYMMDD') >= new moment().format('YYYYMMDD'))
                const sortedArray = eventsNowOn.sort((a, b) => new moment(a.dateTimestamp).format('YYYYMMDD') - new moment(b.dateTimestamp).format('YYYYMMDD'))
                dispatch({ type: 'GET_ALL_EVENTS', allEvents: sortedArray })
            })
    } catch (error) {
        console.log(error);
    }
}


export const getPlaceEvents = async (uid, triggerModal, eventForManage, dispatch) => {
    try {
        let events = []

        await firebase.firestore().collection("events").where('owner', '==', uid)
            .get().then((snapshot) => {
                snapshot.forEach(async (doc) => {
                    events.push({
                        dateDescription: doc.data().dateDescription,
                        name: doc.data().name,
                        photos: doc.data().photos,
                        enableListNames: doc.data().enableListNames,
                        listNames: doc.data().listNames,
                        description: doc.data().description,
                        dateTimestamp: doc.data().dateTimestamp,
                        administrators: doc.data().administrators,
                        id: doc.id,
                        owner: doc.data().owner,
                        list: doc.data().list
                    })
                })
                moment.locale('pt-br')
                const eventsNowOn = events.filter((x) => new moment(x.dateTimestamp).format('YYYYMMDD') >= new moment().format('YYYYMMDD'))
                const sortedArray = eventsNowOn.sort((a, b) => new moment(a.dateTimestamp).format('YYYYMMDD') - new moment(b.dateTimestamp).format('YYYYMMDD'))
                if (eventForManage === true) {
                    dispatch({ type: 'GET_PLACEEVENTS_FOR_MANAGE', placeEventsForManage: sortedArray })
                } else {
                    dispatch({ type: 'GET_PLACEEVENTS', placeEvents: sortedArray })
                }
                if (triggerModal === true) {
                    dispatch({ type: 'SET_HOME_MODAL', homeModal: true })
                } else {

                }
            })
        dispatch({ type: 'SET_LOADING_EVENT', loadingEvent: false })
    } catch (error) {
        console.log(error);
    }
}






export const getAdministrativeProfiles = async (uid, dispatch) => {
    try {
        let administrativeProfiles = []
        await firebase.firestore().collection("users").where('employer', '==', uid)
            .get().then((snapshot) => {
                snapshot.forEach(async (doc) => {
                    administrativeProfiles.push({
                        name: doc.data().name,
                        email: doc.data().email,
                        cpf: doc.data().cpf,
                        id: doc.id,
                    })
                })
            })
        dispatch({ type: 'ADMINISTRATIVE_PROFILES', administrativeProfiles: administrativeProfiles })
    } catch (error) {
        console.log(error);
    }
}


export const getMarkers = async (dispatch) => {
    try {
        await firebase.firestore().collection("users").where('userType', '==', 'Professional').get().then((snapshot) => {
            let markers = []
            snapshot.forEach((doc) => {
                markers.push({
                    key: doc.id,
                    coords: doc.data().coords,
                    name: doc.data().name,
                    address: doc.data().address
                })
                dispatch({ type: 'GET_MARKERS', markers: markers })
            })
        });
    } catch (error) {
        console.log(error);
    }
}











// LOGIN E LOGOUT


export const handleSignIn = async (email, password, dispatch, navigation) => {
    try {
        dispatch({ type: 'SET_LOADING_AUTH', loadingAuth: true })
        await firebase.auth().signInWithEmailAndPassword(email, password)
            .then((user) => {
                dispatch({ type: 'SET_LOADING_AUTH', loadingAuth: false })
                navigation.dispatch(StackActions.reset({
                    index: 0, actions: [NavigationActions.navigate({ routeName: 'HomeDrawer' })]
                }))
            })
    } catch (error) {
        dispatch({ type: 'SET_LOADING_AUTH', loadingAuth: false })
        switch (error.code) {
            case 'auth/invalid-email': alert('Email inválido')
                break;
            case 'auth/user-not-found': alert('Usuário não existe')
                break;
            case 'auth/wrong-password': alert('Usuário e/ou senha inválidos')
                break;

        }
    }
}

export const handleSignUp = async (name, email, cpf, password, dispatch, navigation) => {
    try {
        dispatch({ type: 'SET_LOADING_AUTH', loadingAuth: true })
        await firebase.auth().createUserWithEmailAndPassword(email, password)
            .then(async (user) => {
                let uid = firebase.auth().currentUser.uid;
                await firebase.firestore().collection("users").doc(uid).set({
                    name: name,
                    email: email,
                    userType: 'Personal',
                    cpf: cpf,
                })
                dispatch({ type: 'SET_LOADING_AUTH', loadingAuth: false })
                navigation.dispatch(StackActions.reset({
                    index: 0, actions: [NavigationActions.navigate({ routeName: 'Preload' })]
                }))
            })
    }

    catch (error) {
        dispatch({ type: 'SET_LOADING_AUTH', loadingAuth: false })
        switch (error.code) {
            case 'auth/email-already-in-use': alert('Email já cadastrado!')
                break;
            case 'auth/invalid-email': alert('Email inválido')
                break;
            case 'auth/operation-not-allowed': alert('Tente novamente')
                break;
            case 'auth/weak-password': alert('Senha muito fraca! Insira uma senha mais forte')
                break;
        }
    }
}



export const handleSignUpAdministrative = async (name, email, cpf, password, employer, dispatch, navigation) => {
    try {
        dispatch({ type: 'SET_LOADING_AUTH', loadingAuth: true })
        await firebase.auth().createUserWithEmailAndPassword(email, password)
            .then(async (user) => {
                let uid = firebase.auth().currentUser.uid;
                await firebase.firestore().collection("users").doc(uid).set({
                    name: name,
                    email: email,
                    userType: 'Administrative',
                    cpf: cpf,
                    employer: employer
                })
            }).then(async () => {
                await firebase.auth().signOut().then(function () {
                    dispatch({ type: 'SET_LOADING_AUTH', loadingAuth: false })
                    navigation.dispatch(StackActions.reset({
                        index: 0, actions: [NavigationActions.navigate({ routeName: 'Preload' })]
                    }))
                }).catch(function (error) {
                    alert(error)
                })
            })
    }
    catch (error) {
        dispatch({ type: 'SET_LOADING_AUTH', loadingAuth: false })
        switch (error.code) {
            case 'auth/email-already-in-use': alert('Email já cadastrado!')
                break;
            case 'auth/invalid-email': alert('Email inválido')
                break;
            case 'auth/operation-not-allowed': alert('Tente novamente')
                break;
            case 'auth/weak-password': alert('Senha muito fraca! Insira uma senha mais forte')
                break;
        }
    }
}

export const handleSignUpBusiness = async (name, email, cnpj, password, coords, address, dispatch, navigation) => {
    try {
        dispatch({ type: 'SET_LOADING_AUTH', loadingAuth: true })
        await firebase.auth().createUserWithEmailAndPassword(email, password)
            .then(async (user) => {
                let uid = firebase.auth().currentUser.uid;
                await firebase.firestore().collection("users").doc(uid).set({
                    name: name,
                    email: email,
                    userType: 'Professional',
                    address: address,
                    coords: coords,
                    cnpj: cnpj,
                })
                dispatch({ type: 'SET_LOADING_AUTH', loadingAuth: false })
                navigation.dispatch(StackActions.reset({
                    index: 0, actions: [NavigationActions.navigate({ routeName: 'Preload' })]
                }))
            })
    }
    catch (error) {
        dispatch({ type: 'SET_LOADING_AUTH', loadingAuth: false })
        switch (error.code) {
            case 'auth/email-already-in-use': alert('Email já cadastrado!')
                break;
            case 'auth/invalid-email': alert('Email inválido')
                break;
            case 'auth/operation-not-allowed': alert('Tente novamente')
                break;
            case 'auth/weak-password': alert('Senha muito fraca! Insira uma senha mais forte')
                break;
        }
    }
}


export const handleLogOut = (navigation, dispatch) => {
    firebase.auth().signOut()
    navigation.dispatch(StackActions.reset({
        index: 0, actions: [NavigationActions.navigate({ routeName: 'AuthStack' })]
    }))
}












// EDIT USER PROFILE


export const handleChangeName = async (uid, name, dispatch, navigation) => {
    try {
        dispatch({ type: 'SET_LOADING_AUTH', loadingAuth: true })
        await firebase.firestore().collection('users').doc(uid).set({
            name: name
        }, { merge: true })
            .then(() => {
                getUserInfo(dispatch)
                dispatch({ type: 'SET_LOADING_AUTH', loadingAuth: false })
                alert('Nome alterado com sucesso!')
                navigation.navigate('Profile')
            })
    } catch (error) {
        dispatch({ type: 'SET_LOADING_AUTH', loadingAuth: false })
        alert(error)
    }
}


export const handleChangePassword = (currentPassword, newPassword, dispatch, navigation) => {
    dispatch({ type: 'SET_LOADING_AUTH', loadingAuth: true })
    const user = firebase.auth().currentUser
    const credential = firebase.auth.EmailAuthProvider.credential(
        user.email,
        currentPassword
    );
    user.reauthenticateWithCredential(credential).then(function () {
        user.updatePassword(newPassword).then(() => {
            dispatch({ type: 'SET_LOADING_AUTH', loadingAuth: false })
            alert("Sua senha foi alterada!")
            navigation.navigate('Profile')
        }).catch((error) => {
            dispatch({ type: 'SET_LOADING_AUTH', loadingAuth: false })
            alert(error)
        })
    }).catch(function (error) {
        dispatch({ type: 'SET_LOADING_AUTH', loadingAuth: false })
        alert(error)
    });
}


export const handleChangeEmail = (currentPassword, newEmail, dispatch, navigation, uid) => {
    dispatch({ type: 'SET_LOADING_AUTH', loadingAuth: true })
    const user = firebase.auth().currentUser
    const credential = firebase.auth.EmailAuthProvider.credential(
        user.email,
        currentPassword
    );
    user.reauthenticateWithCredential(credential).then(function () {
        user.updateEmail(newEmail).then(async () => {
            try {
                await firebase.firestore().collection('users').doc(uid).set({
                    email: newEmail
                }, { merge: true })
                    .then(() => {
                        getUserInfo(dispatch)
                    })
            } catch (error) {
                dispatch({ type: 'SET_LOADING_AUTH', loadingAuth: false })
                alert(error)
            }
            dispatch({ type: 'SET_LOADING_AUTH', loadingAuth: false })
            alert("Seu email foi alterado!")
            navigation.navigate('Profile')
        }).catch((error) => {
            dispatch({ type: 'SET_LOADING_AUTH', loadingAuth: false })
            alert(error)
        })
    }).catch(function (error) {
        dispatch({ type: 'SET_LOADING_AUTH', loadingAuth: false })
        alert(error)
    })
}


export const handleDeleteAccount = (currentPassword, dispatch, navigation, uid) => {
    dispatch({ type: 'SET_LOADING_AUTH', loadingAuth: true })
    const user = firebase.auth().currentUser
    const credential = firebase.auth.EmailAuthProvider.credential(
        user.email,
        currentPassword
    )
    user.reauthenticateWithCredential(credential).then(function () {
        user.delete().then(async () => {
            try {
                await firebase.firestore().collection('users').doc(uid).delete().then(function () {
                }).catch(function (error) {
                })
            } catch (error) {
                alert(error)
            }
            dispatch({ type: 'SET_LOADING_AUTH', loadingAuth: false })
            navigation.dispatch(StackActions.reset({
                index: 0, actions: [NavigationActions.navigate({ routeName: 'AuthStack' })]
            }))
        }).catch((error) => {
            dispatch({ type: 'SET_LOADING_AUTH', loadingAuth: false })
            alert(error)
        })
    }).catch(function (error) {
        dispatch({ type: 'SET_LOADING_AUTH', loadingAuth: false })
        alert(error)
    })
}