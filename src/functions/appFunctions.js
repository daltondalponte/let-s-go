import firebase from '../services/firebase'
import ImagePicker from 'react-native-image-picker'
import { getPlaceData, getPlaceEvents } from './authFunctions'
import { StackActions, NavigationActions } from 'react-navigation'


// HANDLE PROFILE PHOTOS

export const uploadPhotoToProfile = (uid, dispatch) => {

    ImagePicker.launchImageLibrary({ noData: true }, (imagePickerResponse) => {
        if (imagePickerResponse.didCancel) {
            console.log('ImagePicker Closed')
        } else if (imagePickerResponse.error) {
            console.log('An error occurred: ', error);
        } else {
            save(imagePickerResponse)
        }
    })
    const save = async (imagePickerResponse) => {
        const path = imagePickerResponse.path
        const ref = await firebase.storage().ref('/photos').child(imagePickerResponse.fileName)

        ref.putFile(path).on(
            firebase.storage.TaskEvent.STATE_CHANGED,
            (snapshot) => {
                const progress = Math.round(snapshot.bytesTransferred / snapshot.totalBytes * 100)
                switch (snapshot.state) {
                    case 'running':
                        dispatch({ type: 'PHOTO_UPLOAD_PROGRESS', photoProgress: { loading: true, progress } })
                        break;
                    case 'success':
                        snapshot.ref.getDownloadURL().then(downloadURL => {
                            const fileName = imagePickerResponse.fileName
                            const fileUrl = downloadURL
                            dispatch({ type: 'PHOTO_UPLOAD_PROGRESS', photoProgress: { loading: false, progress } })
                            uploadProfilePhotoToFirestore(uid, fileName, fileUrl, dispatch)
                        })
                        break;
                    default:
                        break;
                }
            })
    }
    const uploadProfilePhotoToFirestore = async (uid, fileName, fileUrl, dispatch) => {
        try {
            await firebase.firestore().collection('users').doc(uid).set({
                photos: firebase.firestore.FieldValue.arrayUnion({ fileName, fileUrl }),
            }, { merge: true })
        } catch (error) {
            alert(error)
        }
        const triggerModal = false
        const eventForManage = false
        getPlaceData(uid, triggerModal, eventForManage, dispatch)
    }
}


export const deleteProfilePhotos = async (photo, uid, dispatch) => {
    const triggerModal = false
    const eventForManage = false
    try {
        await firebase.firestore().collection("users").doc(uid).update({
            photos: firebase.firestore.FieldValue.arrayRemove(photo)
        })
            .catch((error) => {
                alert(error)
            });
    } catch (error) {
        alert(error);
    }
    const ref = firebase.storage().ref('photos/' + photo.fileName);
    ref.delete().then(function () {
        getPlaceData(uid, triggerModal, eventForManage, dispatch)
    }).catch(function (error) {
        alert(error)
    })
}













// HANDLE EVENT

export const imagePickForEvent = (dispatch) => {
    ImagePicker.launchImageLibrary({ noData: true }, (imagePickerResponse) => {
        if (imagePickerResponse.didCancel) {
            console.log('ImagePicker Closed')
        } else if (imagePickerResponse.error) {
            console.log('An error occurred: ', error);
        } else {
            dispatch({ type: 'IMAGEPICKER_RESPONSE', imagePickerResponse: imagePickerResponse })
        }
    })
}

export const handleCreateEvent = async (name, description, dateDescription, dateTimestamp, placeUid, list, enableListNames, imagePickerResponse, navigation, dispatch) => {
    if (name && description && dateDescription && imagePickerResponse) {
        const path = imagePickerResponse.path
        const ref2 = await firebase.storage().ref('/photos').child(imagePickerResponse.fileName)

        ref2.putFile(path).on(
            firebase.storage.TaskEvent.STATE_CHANGED,
            (snapshot) => {
                const progress = Math.round(snapshot.bytesTransferred / snapshot.totalBytes * 100)
                switch (snapshot.state) {
                    case 'running':
                        console.log(snapshot)
                        dispatch({ type: 'PHOTO_UPLOAD_PROGRESS', photoProgress: { loading: true, progress } })
                        break;
                    case 'success':
                        console.log(snapshot)
                        snapshot.ref.getDownloadURL().then(downloadURL => {
                            const fileName = imagePickerResponse.fileName
                            const fileUrl = downloadURL
                            uploadEventPhotoToFirebase(name, description, dateDescription, dateTimestamp, placeUid, enableListNames, fileName, fileUrl, navigation, dispatch, progress)
                        })
                        break;
                }
            })
    } else {
        alert('Preencha todos os campos e carregue uma foto')
    }

    const uploadEventPhotoToFirebase = async (name, description, dateDescription, dateTimestamp, placeUid, enableListNames, fileName, fileUrl, navigation, dispatch, progress) => {
        try {
            await firebase.firestore().collection("events").doc(name + ' - ' + dateTimestamp).set({
                name: name,
                description: description,
                dateDescription: dateDescription,
                dateTimestamp: dateTimestamp,
                enableListNames: enableListNames,
                listNames: [],
                administrators: [],
                owner: placeUid,
                list: list,
                photos: firebase.firestore.FieldValue.arrayUnion({ fileName, fileUrl }),
            })
            dispatch({ type: 'PHOTO_UPLOAD_PROGRESS', photoProgress: { loading: false, progress } })
            const triggerModal = false
            const eventForManage = false
            getPlaceEvents(placeUid, triggerModal, eventForManage, dispatch)
            navigation.dispatch(StackActions.reset({
                index: 0, actions: [NavigationActions.navigate({ routeName: 'ProfileTab' })]
            }))
            alert('Evento Criado com sucesso!')
        } catch (error) {
            console.log(error);
        }
    }
}














// EDIT EVENT

export const handleChangeEventName = async (eventId, placeUid, name, dispatch, navigation) => {
    try {
        dispatch({ type: 'SET_LOADING_EVENT', loadingEvent: true })
        await firebase.firestore().collection('events').doc(eventId).set({
            name: name
        }, { merge: true })
            .then(() => {
                const triggerModal = false
                const eventForManage = true
                getPlaceEvents(placeUid, triggerModal, eventForManage, dispatch)
                dispatch({ type: 'SET_LOADING_EVENT', loadingEvent: false })
                alert('Nome alterado com sucesso!')
            })
    } catch (error) {
        dispatch({ type: 'SET_LOADING_EVENT', loadingEvent: false })
        alert(error)
    }
}


export const handleChangeEventDescription = async (eventId, placeUid, description, dispatch, navigation) => {
    try {
        dispatch({ type: 'SET_LOADING_EVENT', loadingEvent: true })
        await firebase.firestore().collection('events').doc(eventId).set({
            description: description
        }, { merge: true })
            .then(() => {
                const triggerModal = false
                const eventForManage = true
                getPlaceEvents(placeUid, triggerModal, eventForManage, dispatch)
                dispatch({ type: 'SET_LOADING_EVENT', loadingEvent: false })
                alert('Descrição alterada com sucesso!')
            })
    } catch (error) {
        dispatch({ type: 'SET_LOADING_EVENT', loadingEvent: false })
        alert(error)
    }
}


export const handleChangeEventDate = async (eventId, placeUid, dateDescription, dateTimestamp, dispatch, navigation) => {
    try {
        dispatch({ type: 'SET_LOADING_EVENT', loadingEvent: true })
        await firebase.firestore().collection('events').doc(eventId).set({
            dateDescription: dateDescription,
            dateTimestamp: dateTimestamp
        }, { merge: true })
            .then(() => {
                const triggerModal = false
                const eventForManage = true
                getPlaceEvents(placeUid, triggerModal, eventForManage, dispatch)
                dispatch({ type: 'SET_LOADING_EVENT', loadingEvent: false })
                alert('Descrição alterada com sucesso!')
            })
    } catch (error) {
        dispatch({ type: 'SET_LOADING_EVENT', loadingEvent: false })
        alert(error)
    }
}

export const handleChangeEventPhoto = async (eventId, placeUid, imagePickerResponse, oldPhotoForDelete, dispatch, navigation) => {
    if (imagePickerResponse) {
        const ref = firebase.storage().ref('photos/' + oldPhotoForDelete[0].fileName);
        ref.delete().then(async () => {
            const path = imagePickerResponse.path
            const ref2 = await firebase.storage().ref('/photos').child(imagePickerResponse.fileName)
            ref2.putFile(path).on(
                firebase.storage.TaskEvent.STATE_CHANGED,
                (snapshot) => {
                    const progress = Math.round(snapshot.bytesTransferred / snapshot.totalBytes * 100)
                    switch (snapshot.state) {
                        case 'running':
                            console.log(snapshot)
                            dispatch({ type: 'PHOTO_UPLOAD_PROGRESS', photoProgress: { loading: true, progress } })
                            break;
                        case 'success':
                            snapshot.ref.getDownloadURL().then(downloadURL => {
                                const fileName = imagePickerResponse.fileName
                                const fileUrl = downloadURL
                                uploadToFirebase(fileName, fileUrl, eventId, placeUid, progress, oldPhotoForDelete, dispatch, navigation)
                            })
                            break;
                    }
                })

        }).catch(function (error) {
            alert(error)
        })
    } else {
        alert('Você precisa selecionar uma nova foto para salvar.')
    }
    const uploadToFirebase = async (fileName, fileUrl, eventId, placeUid, progress, oldPhotoForDelete, dispatch, navigation) => {
        try {
            await firebase.firestore().collection('events').doc(eventId).update({
                photos: firebase.firestore.FieldValue.arrayRemove(oldPhotoForDelete[0])
            }).then(async () => {
                await firebase.firestore().collection('events').doc(eventId).update({
                    photos: firebase.firestore.FieldValue.arrayUnion({ fileName, fileUrl })
                })
            })
            const triggerModal = false
            const eventForManage = true
            getPlaceEvents(placeUid, triggerModal, eventForManage, dispatch)
            dispatch({ type: 'PHOTO_UPLOAD_PROGRESS', photoProgress: { loading: false, progress } })
            dispatch({ type: 'IMAGEPICKER_RESPONSE', imagePickerResponse: '' })
        } catch (error) {
            console.log(error)
        }
    }
}


export const handleSetAdministratorProfile = async (admProfile, event, dispatch) => {
    if (event.id) {
        try {
            await firebase.firestore().collection("events").doc(event.id).set({
                administrators: firebase.firestore.FieldValue.arrayUnion(admProfile)
            }, { merge: true })
            const triggerModal = false
            const eventForManage = true
            getPlaceEvents(event.owner, triggerModal, eventForManage, dispatch)
        } catch (error) {
            console.log(error);
        }
    }
}


export const handleRemoveAdministratorProfileFromEvent = async (admProfile, event, dispatch) => {
    if (event.id) {
        try {
            await firebase.firestore().collection("events").doc(event.id).set({
                administrators: firebase.firestore.FieldValue.arrayRemove(admProfile)
            }, { merge: true })
            const triggerModal = false
            const eventForManage = true
            getPlaceEvents(event.owner, triggerModal, eventForManage, dispatch)
        } catch (error) {
            console.log(error);
        }
    }
}



export const handleDeleteEvent = async (eventId, placeUid, photo, dispatch, navigation) => {
    if (eventId) {
        const ref = firebase.storage().ref('photos/' + photo[0].fileName)
        ref.delete().then(async () => {
            try {
                await firebase.firestore().collection('events').doc(eventId).delete()
                    .then(() => {
                        const triggerModal = false
                        const eventForManage = true
                        getPlaceEvents(placeUid, triggerModal, eventForManage, dispatch)
                        navigation.navigate('Manage')
                    })

            } catch (error) {
                console.log(error)
            }
        }).catch(function (error) {
            alert(error)
        })
    } else {
        alert('Você precisa selecionar uma nova foto para salvar.')
    }
}





















// HANDLE LIST NAMES

export const handleUserSendNameToList = async (event, userName, userUid, placeUid, dispatch, navigation) => {
    if (event.id) {
        try {
            await firebase.firestore().collection("events").doc(event.id).set({
                listNames: firebase.firestore.FieldValue.arrayUnion({ name: userName, uid: userUid, werePresent: false })
            }, { merge: true })
            const triggerModal = false
            const eventForManage = false
            getPlaceEvents(placeUid, triggerModal, eventForManage, dispatch)
        } catch (error) {
            console.log(error);
        }
    }
}

export const handleUserRemoveNameOfList = async (event, userName, userUid, placeUid, dispatch, navigation) => {
    const pos = event.listNames.map((e) => { return e.uid; }).indexOf(userUid);
    const itemToChange = event.listNames[pos]
    if (event.id) {
        try {
            await firebase.firestore().collection("events").doc(event.id).set({
                listNames: firebase.firestore.FieldValue.arrayRemove(itemToChange)
            }, { merge: true })
            const triggerModal = false
            const eventForManage = false
            getPlaceEvents(placeUid, triggerModal, eventForManage, dispatch)
        } catch (error) {
            console.log(error);
        }
    }
}

export const handleSetUserListType = async (selectedNames, event, item, dispatch) => {
    selectedNames.forEach(async (selectedNames) => {
        const pos = event.listNames.map(function (e) { return e.uid; }).indexOf(selectedNames.key);
        const itemToChange = event.listNames[pos]
        try {
            await firebase.firestore().collection('events').doc(event.id).update({
                listNames: firebase.firestore.FieldValue.arrayRemove(itemToChange)
            }).then(async () => {
                await firebase.firestore().collection('events').doc(event.id).update({
                    listNames: firebase.firestore.FieldValue.arrayUnion({
                        name: itemToChange.name,
                        uid: itemToChange.uid,
                        werePresent: itemToChange.werePresent,
                        listType: item.listType,
                        listTypeDescription: item.listTypeDescription
                    })
                })
            })
            const triggerModal = false
            const eventForManage = true
            getPlaceEvents(event.owner, triggerModal, eventForManage, dispatch)
        } catch (error) {
            alert(error)
        }
    })
}

export const handleSetUserConfirmationInEvent = async (selectedNames, event, dispatch) => {
    selectedNames.forEach(async (selectedNames) => {
        const pos = event.listNames.map(function (e) { return e.uid; }).indexOf(selectedNames.key);
        const itemToChange = event.listNames[pos]
        try {
            await firebase.firestore().collection('events').doc(event.id).update({
                listNames: firebase.firestore.FieldValue.arrayRemove(itemToChange)
            }).then(async () => {
                await firebase.firestore().collection('events').doc(event.id).update({
                    listNames: firebase.firestore.FieldValue.arrayUnion({
                        name: itemToChange.name,
                        uid: itemToChange.uid,
                        werePresent: true,
                        listType: itemToChange.listType,
                        listTypeDescription: itemToChange.listTypeDescription
                    })
                })
            })
            const triggerModal = false
            const eventForManage = true
            getPlaceEvents(event.owner, triggerModal, eventForManage, dispatch)
        } catch (error) {
            alert(error)
        }
    })
}


export const handleRemoveUserConfirmationInEvent = async (selectedNames, event, dispatch) => {
    selectedNames.forEach(async (selectedNames) => {
        const pos = event.listNames.map(function (e) { return e.uid; }).indexOf(selectedNames.key);
        const itemToChange = event.listNames[pos]
        try {
            await firebase.firestore().collection('events').doc(event.id).update({
                listNames: firebase.firestore.FieldValue.arrayRemove(itemToChange)
            }).then(async () => {
                await firebase.firestore().collection('events').doc(event.id).update({
                    listNames: firebase.firestore.FieldValue.arrayUnion({
                        name: itemToChange.name,
                        uid: itemToChange.uid,
                        werePresent: false,
                        listType: itemToChange.listType,
                        listTypeDescription: itemToChange.listTypeDescription
                    })
                })
            })
            const triggerModal = false
            const eventForManage = true
            getPlaceEvents(event.owner, triggerModal, eventForManage, dispatch)
        } catch (error) {
            alert(error)
        }
    })
}


export const handleDeleteTypeOfList = async (item, event, dispatch, navigation) => {
    try {
        await firebase.firestore().collection('events').doc(event.id).update({
            list: firebase.firestore.FieldValue.arrayRemove(item)
        })
        const triggerModal = false
        const eventForManage = true
        getPlaceEvents(event.owner, triggerModal, eventForManage, dispatch)
    } catch (error) {
        alert(error)
    }
}

export const handleAddTypeOfList = async (listType, listTypeDescription, event, dispatch, navigation) => {
    try {
        await firebase.firestore().collection('events').doc(event.id).update({
            list: firebase.firestore.FieldValue.arrayUnion({ listType: listType, listTypeDescription: listTypeDescription })
        })
        const triggerModal = false
        const eventForManage = true
        getPlaceEvents(event.owner, triggerModal, eventForManage, dispatch)
    } catch (error) {
        alert(error)
    }
}


















// HANDLE RATINGS

export const handleRating = async (rating, placeUid, userUid) => {
    try {
        await firebase.firestore().collection('ratings').doc(placeUid).set({
            [userUid]: { rating },
        }, { merge: true })
    } catch (error) {
        alert(error);
    }
}

export const getRatings = async () => {
    firebase.firestore().collection('ratings').doc(placeData.placeUid).get().then(function (doc) {
        if (doc.exists) {
            console.log("Document data:", doc.data().rating)
        } else {
            console.log("No such document!");
        }
    }).catch(function (error) {
        console.log("Error getting document:", error);
    })
    const a = [1, 2, 2, 2, 1];
    const result = eval(a.join('+')) / a.length
    const total = Math.round(result)
    //console.log('result'+result,'total'+total)
}













