import React, { useState, useRef, useEffect } from 'react';
import { Text, View, Platform, TextInput, Modal, FlatList } from 'react-native';
import { StackActions, NavigationActions } from 'react-navigation'
import { style } from '../styles'
import MapView, { Marker, PROVIDER_GOOGLE } from 'react-native-maps';
import Geolocation from '@react-native-community/geolocation'
import Geocoder from 'react-native-geocoding'
import { useSelector, useDispatch } from 'react-redux'
import Icon from 'react-native-vector-icons/MaterialCommunityIcons'
import BarButton from '../components/barButton'
import FlatlistEvent from '../screens/businessTabs/flatlistEvent'
import SearchInput from '../components/searchBar'
import MapStyle from '../services/mapStyle'
import { getPlaceData } from '../functions/authFunctions'
import LoadingOnly from '../components/loadingOnly'

import Header from '../components/header'


const Home = (props) => {
    const dispatch = useDispatch()

    const markers = useSelector(state => state.Reducer.markers)
    const placeInfo = useSelector(state => state.Reducer.placeInfo)
    const placeEvents = useSelector(state => state.Reducer.placeEvents)
    const homeModal = useSelector(state => state.Reducer.homeModal)
    const userInfo = useSelector(state => state.Reducer.userInfo)
    const loadingEvent = useSelector(state => state.Reducer.loadingEvent)

    const map = useRef()

    const [triggerModal, setTriggerModal] = useState(true)
    const [eventForManage, setEventForManage] = useState(false)

    const [mapLocation, setMapLocation] = useState({
        center: {
            latitude: 21.788,
            longitude: 47.432
        },
        zoom: 14,
        pitch: 0,
        altitude: 0,
        heading: 0,
    })

    useEffect(() => {
        Geocoder.init('AIzaSyBzwQrdFLxsNTvNO_TtX2jws76dPPk5dMM', { language: 'pt-br' })
        getCurrentLocation()
    }, [])

    const getCurrentLocation = () => {
        Geolocation.getCurrentPosition(async (info) => {
            const geo = await Geocoder.from(info.coords.latitude, info.coords.longitude)
            if (geo.results.length > 0) {
                const loc = {
                    name: geo.results[0].formatted_address,
                    center: {
                        latitude: info.coords.latitude,
                        longitude: info.coords.longitude
                    },
                    zoom: 14,
                    pitch: 0,
                    altitude: 0,
                    heading: 0
                }
                setMapLocation(loc)
            }

        }, (error) => {

        })
    }

    

    const handleGoToPlaceProfile = () => {
        props.navigation.dispatch(StackActions.reset({
            index: 0, actions: [NavigationActions.navigate({ routeName: 'ProfileTab' })]
        }))
    }

    const RenderModal = () => {
        return (
            <Modal animationType="slide" transparent={true} visible={homeModal} onRequestClose={() => { }}>
                <View style={{marginTop: '30%', alignSelf: 'center', width: '90%', height: '70%', borderColor: '#ff7f00', borderWidth: 1, borderRadius: 50, backgroundColor: 'rgba(255,255,255,0.9)'}}>
                    <Text style={[style.headerText, { alignSelf:'center', padding: 20 }]}>{placeInfo.name}</Text>
                    <Text style={[style.titleText, { alignSelf:'center', padding: 20 }]}>Confira os nossos próximos eventos:</Text>

                    <FlatList
                        data={placeEvents}
                        renderItem={({ item }) => <FlatlistEvent event={item} navigation={props.navigation}/>}
                        keyExtractor={(item) => item.id}
                        showsVerticalScrollIndicator={false}
                    />
                    <BarButton position={'absolute'} bottom={20} left={20} label={'Acesse e saiba mais'} width={200} onPress={() => { handleGoToPlaceProfile(), dispatch({ type: 'SET_HOME_MODAL', homeModal: false }) }} />
                    <Icon style={{position: 'absolute', bottom: 20, right: 20}} name="close" size={50} color="#ff7f00" onPress={() => { dispatch({ type: 'SET_HOME_MODAL', homeModal: false }) }} />
                </View>
            </Modal>
        )
    } 


    return (
        <View style={{ flex: 1 }} behavior={(Platform.OS === 'ios') ? "padding" : null} enabled>
            <Header text={'olá,'} title={userInfo.name} onPress={() => { props.navigation.openDrawer() }} />
            <MapView
                style={{ flex: 1 }}
                provider={PROVIDER_GOOGLE}
                ref={map}
                camera={mapLocation}
                showsUserLocation={true}
                showsMyLocationButton={true}
                customMapStyle={MapStyle}
            >
                {markers.map(m => (
                    <Marker
                        key={m.key}
                        image={require('../../assets/images/pin.png')}
                        coordinate={{ latitude: m.coords.lat, longitude: m.coords.lng }}
                        onPress={() => { getPlaceData(m.key, triggerModal, eventForManage, dispatch)}}
                    >
                    </Marker>
                ))}
            </MapView>
            <LoadingOnly position={'absolute'} left={10} top={80} size={'large'} loading={loadingEvent} />
            <RenderModal />
            <SearchInput/>
        </View>
    )
}


export default Home
