import React, { useState, useEffect } from 'react';
import { View, Text, TouchableOpacity, Image, FlatList } from 'react-native';
import { style } from '../../styles'
import { useSelector, useDispatch } from 'react-redux'
import HeaderBusiness from '../../components/headerBusiness'
import FlatlistEvent from '../businessTabs/flatlistEvent'



const Calendar = (props) => {
    const dispatch = useDispatch()

    const placeEvents = useSelector(state => state.Reducer.placeEvents)
    const placeInfo = useSelector(state => state.Reducer.placeInfo)



    return (
        <View style={{ flex: 1 }}>
            <HeaderBusiness label={placeInfo.name} onPress={() => { props.navigation.navigate('HomeDrawer') }} />
            <View style={{ marginBottom: 20, marginLeft: 20 }}>
                <Text style={[style.titleText, { marginTop: 20 }]}>Agenda:</Text>
                <FlatList
                    data={placeEvents}
                    renderItem={({ item }) => <FlatlistEvent event={item} navigation={props.navigation} />}
                    keyExtractor={(item) => item.id}
                    showsVerticalScrollIndicator={true}
                    style={{ marginLeft: -20 }}         />
            </View>
        </View>
    )
}

export default Calendar