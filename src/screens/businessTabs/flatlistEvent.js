import React, { useState, useRef, useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux'
import { Text, View, Image, TouchableOpacity } from 'react-native'
import { style } from '../../styles'
import Icon from "react-native-vector-icons/FontAwesome"


const FlatlistEvent = (props) => {
    const dispatch = useDispatch()

    return (
        <View style={{ flex: 1 }}>
            <TouchableOpacity style={style.dateBox} onPress={() => { props.navigation.navigate('EventPage', { event: props.event }) }}>
                <Text style={[style.titleTextBold, { marginLeft: 20 }]}>{props.event.name}</Text>
                <Icon style={{ marginLeft: 20 }} name="circle" size={15} color="#fff" />
                <Text style={[style.normalText, { marginLeft: 20 }]}>{props.event.dateDescription}</Text>

            </TouchableOpacity>
            <View style={{ marginLeft: 20 }}>
                <Text style={[style.normalTextBold, { marginTop: 10 }]}>Descrição do evento:</Text>
                <Text style={[style.normalText, { marginTop: 10 }]}>{props.event.description}</Text>
            </View>
        </View>
    )
}

export default FlatlistEvent