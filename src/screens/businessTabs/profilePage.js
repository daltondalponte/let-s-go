import React, { useState, useEffect } from 'react';
import { View, Text, TouchableOpacity, Image, FlatList, Modal, Dimensions } from 'react-native';
import { style } from '../../styles'
import Icon from "react-native-vector-icons/MaterialCommunityIcons"
import { useSelector, useDispatch } from 'react-redux'
import BarButton from '../../components/barButton'
import HeaderBusiness from '../../components/headerBusiness'
import { AirbnbRating } from 'react-native-ratings';
import LoadingNumeric from '../../components/loadingNumeric';
import FlatlistEvent from '../businessTabs/flatlistEvent'
import { uploadPhotoToProfile, handleRating, deleteProfilePhotos } from '../../functions/appFunctions';
import { StackActions, NavigationActions } from 'react-navigation'


const ProfilePage = (props) => {
    const dispatch = useDispatch()


    const [modalPhoto, setModalPhoto] = useState(false)
    const [modalPhotoInfo, setModalPhotoInfo] = useState('')

    const placeInfo = useSelector(state => state.Reducer.placeInfo)
    const placeEvents = useSelector(state => state.Reducer.placeEvents)
    const userInfo = useSelector(state => state.Reducer.userInfo)
    const photoProgress = useSelector(state => state.Reducer.photoProgress)

    const screenSize = Math.round(Dimensions.get('window').width)
    const photoSize = screenSize / 4

    const RenderModalPhoto = () => {
        return (
            <Modal
                animationType="slide"
                transparent={true}
                visible={modalPhoto}
                onRequestClose={() => { setModalPhoto(false) }}>
                <View style={{
                    flex: 1,
                    backgroundColor: 'rgba(0,0,0,0.8)'
                }}>
                    <Image resizeMode='contain' style={{ width: '90%', height: '90%', alignSelf: 'center' }} source={{ uri: modalPhotoInfo.fileUrl }}></Image>

                    <TouchableOpacity style={{ position: 'absolute', bottom: 10, left: 20 }} onPress={() => { setModalPhoto(false) }}>
                        <Icon name="close" size={40} color='#fff' />
                    </TouchableOpacity>
                    {userInfo.uid == placeInfo.uid &&
                        <TouchableOpacity style={{ position: 'absolute', bottom: 10, right: 20 }} onPress={() => { deleteProfilePhotos(modalPhotoInfo, placeInfo.uid, dispatch), setModalPhoto(false) }}>
                            <Icon name="delete" size={40} color='#fff' />
                        </TouchableOpacity>
                    }
                </View>
            </Modal>
        )
    }


    return (
        <View style={{ flex: 1 }}>
            <HeaderBusiness label={placeInfo.name} onPress={() => { props.navigation.dispatch(StackActions.reset({
                            index: 0, actions: [NavigationActions.navigate({ routeName: 'HomeDrawer' })]
                        }))}} />
            <LoadingNumeric percentage={photoProgress.progress} loading={photoProgress.loading} />
            <RenderModalPhoto />



            <Text style={[style.titleText, { padding: 10 }]}>Fotos:</Text>
            <View style={{marginBottom: 20 }}>
                <FlatList
                    data={placeInfo.photos}
                    renderItem={({ item }) =>
                            <TouchableOpacity  style={{ height: photoSize, width: photoSize }} onPress={() => { setModalPhotoInfo(item), setModalPhoto(true) }}>
                                <Image style={style.photo} source={{ uri: item.fileUrl }}></Image>
                            </TouchableOpacity>
                    }
                    keyExtractor={(item) => item.fileUrl}
                    horizontal={true}
                    showsHorizontalScrollIndicator={false}
                />
            </View>



            <View style={{ marginBottom: 20, marginLeft: 20 }}>
                <Text style={style.titleTextBold}>Nota do público:</Text>
                <AirbnbRating
                    type='star'
                    ratingCount={6}
                    reviewSize={1}
                    imageSize={30}
                    defaultRating={3}
                    onFinishRating={(rating) => handleRating(rating, placeInfo.uid, userInfo.uid)}
                />
            </View>
            {userInfo.uid == placeInfo.uid &&
            <View style={{flexDirection: 'row', justifyContent: 'space-evenly'}}>
                <BarButton label={'Enviar foto'} marginRight={20} width={150} onPress={() => { uploadPhotoToProfile(placeInfo.uid, dispatch) }} />
                <BarButton label={'Criar Evento'} marginRight={20} width={150} onPress={() => { props.navigation.navigate('CreateEvent'), dispatch({ type: 'IMAGEPICKER_RESPONSE', imagePickerResponse: '' }) }} />

                </View>
            }



            <Text style={[style.titleTextBold, { marginTop: 20, marginLeft: 20 }]}>Agenda:</Text>
            <FlatList
                data={placeEvents}
                renderItem={({ item }) => <FlatlistEvent event={item} navigation={props.navigation} />}
                keyExtractor={(item) => item.id}
                showsVerticalScrollIndicator={true}
            />
        </View>
    )
}

export default ProfilePage




