import React, { useState, useEffect } from 'react'
import { Text, Image, View, Switch, TextInput, ScrollView, TouchableOpacity } from 'react-native'
import { style } from '../../styles'
import CalendarPicker from 'react-native-calendar-picker'
import moment from 'moment'
import { useSelector, useDispatch } from 'react-redux'
import { handleCreateEvent, imagePickForEvent } from '../../functions/appFunctions'
import BarButton from '../../components/barButton'
import LoadingNumeric from '../../components/loadingNumeric'
import Icon from 'react-native-vector-icons/MaterialCommunityIcons'


const CreateEvent = (props) => {
  const dispatch = useDispatch()

  useEffect(() => {
    setName(''), setDescription(''), setList([])
  }, [])


  const [date, setDate] = useState('')
  const minDate = moment(new Date()).format()
  const dateDescription = moment(date).format('LL')
  const dateTimestamp = moment(date).format()

  const [name, setName] = useState('')
  const [description, setDescription] = useState('')
  const [isEnabled, setIsEnabled] = useState(false)

  const [listType, setListType] = useState('')
  const [listTypeDescription, setListTypeDescription] = useState('')
  const [list, setList] = useState([])

  const userInfo = useSelector(state => state.Reducer.userInfo)
  const placeInfo = useSelector(state => state.Reducer.placeInfo)
  const photoProgress = useSelector(state => state.Reducer.photoProgress)
  const imagePickerResponse = useSelector(state => state.Reducer.imagePickerResponse)

  const handleListTypes = (type, desc) => {
    if (type && desc) {
    setList([...list, { listType: type, listTypeDescription: desc }])
  } else {
    alert('Preencha os campos para salvar')
  }
    setListType('')
    setListTypeDescription('')
  }
  return (
    <View>
      <ScrollView >
        <LoadingNumeric percentage={photoProgress.progress} loading={photoProgress.loading} />
        <View style={style.container}>
          <Text style={[style.headerText, { marginTop: 40, marginBottom: 10 }]}>Criação de Evento</Text>
          <Text style={[style.normalText, { marginBottom: 10 }]}>Seleciona a data e preencha os campos para criar um evento</Text>
        </View>
        <View style={{ marginBottom: -30 }}>
          <CalendarPicker
            startFromMonday={true}
            allowRangeSelection={false}
            minDate={minDate}
            weekdays={['Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sab', 'Dom']}
            months={['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro']}
            previousTitle="Mês Anterior"
            nextTitle="Próximo Mês"
            todayBackgroundColor="#FFBE7E"
            selectedDayColor="#ff7f00"
            selectedDayTextColor="#fff"
            scaleFactor={375}
            disabledDatesTextStyle={{ color: '#cccccc' }}
            textStyle={{
              fontFamily: 'MuseoSans-300',
              color: '#7b7b7b',
            }}
            onDateChange={(date, type) => { setDate(date) }}
          />
        </View>
        <View style={style.container}>
          {imagePickerResponse.uri &&
            <Image style={[style.photoBig, { marginBottom: 10, alignSelf: 'center' }]} source={{ uri: imagePickerResponse.uri }}></Image>
          }
          <TextInput style={style.inputText} autoCapitalize='words' placeholder='Nome do evento' underlineColorAndroid='transparent' value={name} onChangeText={t => setName(t)} />
          <TextInput style={style.inputTextMultiLine} multiline={true} numberOfLines={3} placeholder='Descrição do evento' underlineColorAndroid='transparent' value={description} onChangeText={t => setDescription(t)} />
          <BarButton width={200} label={imagePickerResponse.uri ? 'Apagar foto' : 'Carregar foto'} marginBottom={7} opacity={imagePickerResponse.uri ? 0.7 : 1} onPress={imagePickerResponse.uri ? () => { dispatch({ type: 'IMAGEPICKER_RESPONSE', imagePickerResponse: '' }) } : () => { imagePickForEvent(dispatch) }} />
          <View style={{ flexDirection: 'row', alignItems: 'center', marginTop: 10, marginBottom: 10 }}>



            <Text style={style.titleText}>Envio de nomes na lista?</Text>
            <Switch
              trackColor={{ false: "#FFBE7E", true: "#ff7f00" }}
              thumbColor={isEnabled ? "#fff" : "#fff"}
              ios_backgroundColor="#fff"
              onValueChange={() => { setIsEnabled(previousState => !previousState) }}
              value={isEnabled}
            />
          </View>
          {isEnabled == true &&
            <View>
              <Text style={style.normalTextBold}>Descreva os tipos de nomes e seus atributos.</Text>
              <Text style={[style.normalText, { marginBottom: 10 }]}>Ex.: Tipo: "Vip", Descrição: "Entrada free no camarote Backstage". Lembrando que Você
                                                                                                    pode adicionar mais de um tipo de nome na lista.</Text>
              {list.length > 0 &&
              <Text style={[style.normalTextBold, {marginBottom: 10}]}>Você tem {list.length} tipos de listas.</Text>}
              {list.map((item, index) => (
                <View key={index}>
                  <View style={{flexDirection: 'row'}}>
                  <Text style={[style.normalTextBold, { marginBottom: 10 }]}>- </Text>
                  <Text style={[style.normalText, { marginBottom: 10 }]}>{item.listType}</Text>
                  </View>
                </View>
              ))}
              <TextInput style={style.inputText} autoCapitalize='words' placeholder='Tipo' underlineColorAndroid='transparent' value={listType} onChangeText={t => setListType(t)} />
              <TextInput style={style.inputText} placeholder='Descrição' underlineColorAndroid='transparent' value={listTypeDescription} onChangeText={t => setListTypeDescription(t)} />
              <View style={{flexDirection: 'row', alignItems: 'center'}}>
              <Icon name="plus-circle" size={40} color="#ff7f00" onPress={() => { handleListTypes(listType, listTypeDescription) }} />
              <Text style={[style.normalTextBold, {marginLeft: 5}]}>Aperte no ícone para adicionar</Text>
              </View>
            </View>
          }



          <View style={{ borderTopWidth: 1, borderTopColor: '#cccc', marginTop: 40, borderBottomWidth: 1, borderBottomColor: '#cccc' }}>
            <BarButton label={'Salvar evento'} width={250} marginTop={20} marginBottom={20} onPress={() => {
              handleCreateEvent(
                name,
                description,
                dateDescription,
                dateTimestamp,
                userInfo.uid,
                list,
                isEnabled,
                imagePickerResponse,
                props.navigation,
                dispatch)
            }} />
          </View>
        </View>
      </ScrollView>
      <Icon style={style.absoluteRight} name="keyboard-backspace" size={50} color="#b4b4b4" onPress={() => { props.navigation.navigate('ProfilePage') }} />
    </View>

  )
}


export default CreateEvent