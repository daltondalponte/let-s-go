import React, { useState, useEffect } from 'react';
import { Text, View, FlatList, TouchableOpacity, Modal, Image, Dimensions } from 'react-native';
import { style } from '../../styles'
import { useSelector, useDispatch } from 'react-redux'
import Icon from "react-native-vector-icons/MaterialCommunityIcons"
import HeaderBusiness from '../../components/headerBusiness'


const About = (props) => {



    const [modalPhoto, setModalPhoto] = useState(false)
    const [modalPhotoInfo, setModalPhotoInfo] = useState('')

    const placeInfo = useSelector(state => state.Reducer.placeInfo)
    const userInfo = useSelector(state => state.Reducer.userInfo)


    const screenSize = Math.round(Dimensions.get('window').width)
    const photoSize = screenSize / 4


    const RenderModalPhoto = () => {
        return (

            <Modal
                animationType="slide"
                transparent={true}
                visible={modalPhoto}
                onRequestClose={() => { setModalPhoto(false) }}>
                <View style={{
                    flex: 1,
                    backgroundColor: 'rgba(0,0,0,0.8)'
                }}>
                    <Image resizeMode='contain' style={{ width: '90%', height: '90%', alignSelf: 'center' }} source={{ uri: modalPhotoInfo.fileUrl }}></Image>

                    <TouchableOpacity style={{ position: 'absolute', bottom: 10, left: 20 }} onPress={() => { setModalPhoto(false) }}>
                        <Icon name="close" size={40} color='#fff' />
                    </TouchableOpacity>
                    {userInfo.uid == placeInfo.uid &&
                        <TouchableOpacity style={{ position: 'absolute', bottom: 10, right: 20 }} onPress={() => { deleteProfilePhotos(modalPhotoInfo, placeInfo.uid, dispatch), setModalPhoto(false) }}>
                            <Icon name="delete" size={40} color='#fff' />
                        </TouchableOpacity>
                    }
                </View>
            </Modal>
        )
    }

    return (
        <View style={{ flex: 1 }}>
            <HeaderBusiness label={placeInfo.name} onPress={() => { props.navigation.navigate('ProfilePage')}} />
            <RenderModalPhoto />
            <Text style={[style.titleText, { padding: 10 }]}>Fotos:</Text>

            <View style={{marginBottom: 20 }}>
                <FlatList
                    data={placeInfo.photos}
                    renderItem={({ item }) =>
                            <TouchableOpacity  style={{ height: photoSize, width: photoSize }} onPress={() => { setModalPhotoInfo(item), setModalPhoto(true) }}>
                                <Image style={style.photo} source={{ uri: item.fileUrl }}></Image>
                            </TouchableOpacity>
                    }
                    keyExtractor={(item) => item.fileUrl}
                    horizontal={true}
                    showsHorizontalScrollIndicator={false}
                />
            </View>
            <View style={{margin: 20}}>
                <Text style={[style.titleText, { marginBottom: 5 }]}>Descrição</Text>
                <Text style={[style.normalTextBold, { marginBottom: 20 }]}>{placeInfo.email}</Text>
                <Text style={[style.titleText, { marginBottom: 5 }]}>Email</Text>
                <Text style={[style.normalTextBold, { marginBottom: 20 }]}>{placeInfo.email}</Text>
                <Text style={[style.titleText, { marginBottom: 5 }]}>Endereço</Text>
                <Text style={style.normalTextBold}>{placeInfo.address}</Text>
            </View>

        </View>
    )
}
export default About
