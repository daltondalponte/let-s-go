import React, { useState, useEffect } from 'react';
import { View, Text, TouchableOpacity, Image, FlatList, Modal } from 'react-native';
import { style } from '../../styles'
import Icon from "react-native-vector-icons/MaterialCommunityIcons"
import { useSelector, useDispatch } from 'react-redux'
import BarButton from '../../components/barButton'
import { handleUserSendNameToList, handleUserRemoveNameOfList } from '../../functions/appFunctions'


const EventPage = (props) => {

    const dispatch = useDispatch()
    const eventId = props.navigation.state.params.event
    const placeEvents = useSelector(state => state.Reducer.placeEvents)

    const event = placeEvents.find( ({ id }) => id === eventId.id )

    const userInfo = useSelector(state => state.Reducer.userInfo)
    const placeInfo = useSelector(state => state.Reducer.placeInfo)

    const [modalPhoto, setModalPhoto] = useState(false)

    const listNameCheck = event.listNames ? event.listNames.some(item => item.uid === userInfo.uid) : ''


    console.log('event params', event)
    console.log('filtrado', event)

    const RenderModalPhoto = () => {
        return (

            <Modal
                animationType="slide"
                transparent={true}
                visible={modalPhoto}
                onRequestClose={() => { setModalPhoto(false) }}>
                <View style={{
                    flex: 1,
                    backgroundColor: 'rgba(0,0,0,0.8)'
                }}>
                    <Image resizeMode='contain' style={{ width: '90%', height: '90%', alignSelf: 'center' }} source={{ uri: event.photos[0].fileUrl }}></Image>
                    <TouchableOpacity style={{ position: 'absolute', bottom: 10, left: 20 }} onPress={() => { setModalPhoto(false) }}>
                        <Icon name="close" size={40} color='#fff' />
                    </TouchableOpacity>
                </View>
            </Modal>
        )
    }


    return (
        <View style={{ flex: 1 }}>
            <RenderModalPhoto />
            <TouchableOpacity onPress={() => { setModalPhoto(true) }}>
                <Image resizeMode='cover' style={style.photoEvent} source={{ uri: event.photos[0].fileUrl }}></Image>
            </TouchableOpacity>
            <View style={style.dateBox}>
                <Text style={[style.titleTextBold, { marginLeft: 20 }]}>{event.name}</Text>
                <Icon style={{ marginLeft: 20 }} name="circle" size={15} color="#fff" />
                <Text style={[style.normalText, { marginLeft: 20 }]}>{event.dateDescription}</Text>
            </View>
            <View style={{ marginLeft: 20 }}>
                <Text style={[style.normalTextBold, { marginTop: 10 }]}>Descrição do evento:</Text>
                <Text style={[style.normalText, { marginTop: 10 }]}>{event.description}</Text>

                {event.enableListNames == true &&
                    <View>
                        {listNameCheck===false && <Text style={[style.normalTextBold, { marginTop: 40 }]}>Deseja enviar seu nome na lista?</Text>}
                        <View style={{flexDirection: 'row', alignItems: 'center'}}>
                        <BarButton width={250} label={listNameCheck===true ? 'Seu nome está na lista' : 'Enviar nome na lista'} marginTop={10} opacity={listNameCheck===true ? 0.7 : 1} onPress={listNameCheck===false ? () => { handleUserSendNameToList(event, userInfo.name, userInfo.uid, placeInfo.uid, dispatch, props.navigation) } : () => { handleUserRemoveNameOfList(event, userInfo.name, userInfo.uid, placeInfo.uid, dispatch, props.navigation)}} />
                        {listNameCheck===true && <Icon name="check" size={50} color='#008000' />}
                        </View>
                        {listNameCheck===true && <Text style={[style.normalTextBold, { marginTop: 10 }]}>Para remover seu nome na lista, aperte no mesmo botão</Text>}
                    </View>
                }
            </View>
            <Icon style={style.absoluteRight} name="keyboard-backspace" size={50} color="#b4b4b4" onPress={() => { props.navigation.navigate('ProfilePage') }} />

        </View>
    )
}

export default EventPage




