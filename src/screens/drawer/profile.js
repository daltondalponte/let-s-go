import React, { Component } from 'react';
import { Text, View } from 'react-native';
import BarButton from '../../components/barButton'
import { style } from '../../styles'
import { useSelector, useDispatch } from 'react-redux'
import Header from '../../components/header'
import Icon from "react-native-vector-icons/MaterialCommunityIcons"



const Profile = (props) => {


    const userInfo = useSelector(state => state.Reducer.userInfo)


    return (
        <View style={{ flex: 1 }}>
            <Header text={'Bem vindo ao'} title={'perfil_'} onPress={() => { props.navigation.openDrawer() }} />

            <View style={style.container}>
            <Text style={style.headerText}>Seus dados</Text>
            <Text style={[style.normalText, {marginBottom: 40}]}>Aqui estão os seus dados cadastrados. Você pode Alterar
                                                                 alguns desses dados, entrando em Editar/Alterar Perfil</Text>

                <Text style={style.titleText}>Nome</Text>
                <Text style={[style.normalText, { marginBottom: 20 }]}>{userInfo.name}</Text>
                <Text style={style.titleText}>Email</Text>
                <Text style={[style.titleText, { marginBottom: 20 }]}>{userInfo.email}</Text>
                <Text style={style.titleText}>{(userInfo.userType == 'Professional') ? 'CNPJ' : 'CPF'}</Text>
                <Text style={[style.titleText, { marginBottom: 20 }]}>{(userInfo.userType == 'Professional') ? userInfo.cnpj : userInfo.cpf}</Text>
                {userInfo.userType == 'Professional' &&
                    <Text style={style.titleText}>Endereço</Text>}
                    <Text style={style.normalText}>{userInfo.address}</Text>
                    <BarButton marginTop={40} marginBottom={20} label={'Editar/Alterar Perfil'} width={200} onPress={() => props.navigation.navigate('EditProfile')} />
            </View>
            <Icon style={style.absoluteRight} name="keyboard-backspace" size={50} color="#b4b4b4" onPress={() => { props.navigation.navigate('Home') }} />

        </View>
    )
}
export default Profile
