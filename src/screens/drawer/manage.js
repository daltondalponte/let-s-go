import React, { Component } from 'react';
import { Text, View, FlatList, TouchableOpacity } from 'react-native';
import BarButton from '../../components/barButton'
import { style } from '../../styles'
import Icon from 'react-native-vector-icons/MaterialCommunityIcons'
import { useSelector, useDispatch } from 'react-redux'
import Header from '../../components/header'
import LoadingOnly from '../../components/loadingOnly'


const Manage = (props) => {

    const placeEventsForManage = useSelector(state => state.Reducer.placeEventsForManage)
    const userInfo = useSelector(state => state.Reducer.userInfo)
    const loadingEvent = useSelector(state => state.Reducer.loadingEvent)

    return (
        <View style={{ flex: 1 }}>
            <Header text={'Bem vindo ao'} title={'_gerenciamento'} onPress={() => { props.navigation.openDrawer() }} />
            <LoadingOnly position={'absolute'} right={10} top={80} size={'large'} loading={loadingEvent} />
            <FlatList
                data={placeEventsForManage}
                renderItem={({ item }) => <View style={{ flex: 1 }}>
                    <View style={style.dateBox}>
                        <Text style={[style.titleTextBold, { marginLeft: 20 }]}>{item.name}</Text>
                        <Icon style={{ marginLeft: 20 }} name="circle" size={15} color="#fff" />
                        <Text style={[style.normalText, { marginLeft: 20 }]}>{item.dateDescription}</Text>
                    </View>
                    <Text style={[style.normalText, { position: 'absolute', right: 20, top: 60 }]} onPress={() => { props.navigation.navigate('EditEvent', { event: item }) }}>Editar</Text>

                    <View style={style.container}>
                        <Text style={[style.normalTextBold, { marginTop: 10 }]}>Descrição do evento:</Text>
                        <Text style={[style.normalText, { marginTop: 10, marginBottom: 10 }]}>{item.description}</Text>
                        <BarButton label={'Gerenciar nomes da lista'} marginBottom={10} width={250} onPress={() => { props.navigation.navigate('ListManage', { event: item }) }} />
                    </View>

                </View>}
                keyExtractor={(item) => item.id}
                showsVerticalScrollIndicator={false}
            />
            <Icon style={style.absoluteRight} name="keyboard-backspace" size={50} color="#b4b4b4" onPress={() => { props.navigation.navigate('Home') }} />
        </View>
    )
}
export default Manage
