import React, { useState, useEffect } from 'react';
import { Text, StyleSheet, View, KeyboardAvoidingView, TextInput, ScrollView } from 'react-native';
import BarButton from '../../components/barButton'
import { style } from '../../styles'
import { useSelector, useDispatch } from 'react-redux'
import Header from '../../components/header'
import { handleChangeName, handleChangePassword, handleChangeEmail, handleDeleteAccount } from '../../functions/authFunctions'
import Loading from '../../components/loading'
import Icon from 'react-native-vector-icons/MaterialCommunityIcons'


const EditProfile = (props) => {
    const dispatch = useDispatch()

    const [newName, setNewName] = useState('')
    const [currentPassword, setCurrentPassword] = useState('')
    const [currentPassword2, setCurrentPassword2] = useState('')
    const [currentPassword3, setCurrentPassword3] = useState('')
    const [newEmail, setNewEmail] = useState('')
    const [newPassword, setNewPassword] = useState('')



    const userInfo = useSelector(state => state.Reducer.userInfo)
    const loadingAuth = useSelector(state => state.Reducer.loadingAuth)


    return (
        <View style={{ flex: 1 }}>
            <Header text={'Bem vindo à'} title={'_edição de perfil'} onPress={() => { props.navigation.openDrawer() }} />
            <View><Loading loading={loadingAuth} /></View>

            <ScrollView style={{ flex: 1 }}>
                <View style={[style.container, { marginTop: 20 }]}>


                    <Text style={style.headerText}>Alterar Nome</Text>
                    <View style={{ flexDirection: 'row' }}>
                        <Text style={style.normalText}>Seu nome atual: </Text>
                        <Text style={[style.normalTextBold, { marginBottom: 10 }]}>{userInfo.userName}</Text>
                    </View>
                    <TextInput style={style.inputText} placeholder='Insira o novo nome' underlineColorAndroid='transparent' value={newName} onChangeText={t => setNewName(t)} />
                    <BarButton marginBottom={40} label={'Salvar'} width={150} onPress={() => { handleChangeName(userInfo.uid, newName, dispatch, props.navigation), setNewName('') }} />


                    <Text style={style.headerText}>Alterar Email</Text>
                    <Text style={style.normalText}>Para alterar seu email, é necessário que você insira a sua senha atual. </Text>
                    <View style={{ flexDirection: 'row' }}>
                        <Text style={style.normalText}>Seu email atual: </Text>
                        <Text style={[style.normalTextBold, { marginBottom: 10 }]}>{userInfo.userEmail}</Text>
                    </View>
                    <TextInput style={style.inputText} secureTextEntry placeholder='Insira sua senha atual' underlineColorAndroid='transparent' value={currentPassword} onChangeText={t => setCurrentPassword(t)} />
                    <TextInput style={style.inputText} placeholder='Insira o novo email' underlineColorAndroid='transparent' value={newEmail} onChangeText={t => setNewEmail(t)} />
                    <BarButton marginBottom={40} label={'Salvar'} width={150} onPress={() => { handleChangeEmail(currentPassword, newEmail, dispatch, props.navigation, userInfo.uid), setCurrentPassword(''), setNewEmail('') }} />


                    <Text style={style.headerText}>Alterar Senha</Text>
                    <Text style={style.normalText}>Para alterar sua senha, é necessário que você insira a sua senha atual. </Text>
                    <View style={{ flexDirection: 'row' }}>
                        <Text style={style.normalText}>Sua senha atual: </Text>
                        <Text style={[style.normalTextBold, { marginBottom: 10 }]}>******</Text>
                    </View>
                    <TextInput style={style.inputText} secureTextEntry placeholder='Insira sua senha atual' underlineColorAndroid='transparent' value={currentPassword2} onChangeText={t => setCurrentPassword2(t)} />
                    <TextInput style={style.inputText} secureTextEntry placeholder='Insira sua nova senha' underlineColorAndroid='transparent' value={newPassword} onChangeText={t => setNewPassword(t)} />
                    <BarButton marginBottom={40} label={'Salvar'} width={150} onPress={() => { handleChangePassword(currentPassword2, newPassword, dispatch, props.navigation), setCurrentPassword2(''), setNewPassword('') }} />


                    <Text style={style.headerText}>Excluir conta</Text>
                    <Text style={style.normalText}>Para excluir sua conta, é necessário que você insira a sua senha atual. </Text>
                    <View style={{ flexDirection: 'row' }}>
                        <Text style={style.normalText}>Sua senha atual: </Text>
                        <Text style={[style.normalTextBold, { marginBottom: 10 }]}>******</Text>
                    </View>
                    <TextInput style={style.inputText} secureTextEntry placeholder='Insira sua senha atual' underlineColorAndroid='transparent' value={currentPassword3} onChangeText={t => setCurrentPassword3(t)} />
                    <BarButton marginBottom={10} label={'Excluir conta'} width={150} onPress={() => { handleDeleteAccount(currentPassword3, dispatch, props.navigation, userInfo.uid), setCurrentPassword3(''), setNewEmail('') }} />
                    <Text style={[style.normalText, { marginBottom: 20 }]}>Ao excluir sua conta, você apaga todos os seus dados em nosso aplicativo e site e não será possivel recuperá-los depois.</Text>
                </View>
            </ScrollView>
            <Icon style={style.absoluteRight} name="keyboard-backspace" size={50} color="#b4b4b4" onPress={() => { props.navigation.navigate('Profile') }} />
        </View>
    )
}
export default EditProfile
