import React, { useState, useEffect } from 'react';
import { Text, Image, View, Modal, TextInput, ScrollView, FlatList, TouchableOpacity } from 'react-native';
import BarButton from '../../components/barButton'
import { style } from '../../styles'
import { useSelector, useDispatch } from 'react-redux'
import Header from '../../components/header'
import { handleChangeEventName, handleChangeEventDescription, handleChangeEventDate, imagePickForEvent, handleChangeEventPhoto, handleDeleteEvent, handleDeleteTypeOfList, handleAddTypeOfList, handleSetAdministratorProfile, handleRemoveAdministratorProfileFromEvent } from '../../functions/appFunctions'
import Loading from '../../components/loading'
import Icon from "react-native-vector-icons/MaterialCommunityIcons"
import CalendarPicker from 'react-native-calendar-picker'
import moment from 'moment'
import LoadingNumeric from '../../components/loadingNumeric'


const EditEvent = (props) => {
    const dispatch = useDispatch()

    const [date, setDate] = useState('')
    const [modal, setModal] = useState(false)
    const minDate = moment(new Date()).format()
    const dateDescription = moment(date).format('LL')
    const dateTimestamp = moment(date).format()


    const eventId = props.navigation.state.params.event
    const placeEventsForManage = useSelector(state => state.Reducer.placeEventsForManage)
    const administrativeProfiles = useSelector(state => state.Reducer.administrativeProfiles)

    const event = placeEventsForManage.find(({ id }) => id === eventId.id);


    const [newName, setNewName] = useState('')
    const [description, setDescription] = useState(event.description)


    const [listType, setListType] = useState('')
    const [listTypeDescription, setListTypeDescription] = useState('')
    const [enableListInput, setEnableListInput] = useState(false)
    const [modalConfirm, setModalConfirm] = useState(false)
    const [admProfileData, setAdmProfileData] = useState('')


    const loadingEvent = useSelector(state => state.Reducer.loadingEvent)
    const imagePickerResponse = useSelector(state => state.Reducer.imagePickerResponse)
    const photoProgress = useSelector(state => state.Reducer.photoProgress)

    const administratorCheck = event.administrators ? event.administrators.some(item => item.id === admProfileData.id) : ''

    const ModalDate = () => {
        return (
            <Modal animationType="slide" transparent={true} visible={modal} onRequestClose={() => { }}>
                <View style={{ marginTop: '30%', alignSelf: 'center', width: '90%', height: '50%', borderColor: '#ff7f00', borderWidth: 1, borderRadius: 50, backgroundColor: 'rgba(255,255,255,0.9)' }}>
                    <View style={{ marginTop: 50, }}>
                        <CalendarPicker
                            width={350}
                            height={350}
                            startFromMonday={true}
                            allowRangeSelection={false}
                            minDate={minDate}
                            weekdays={['Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sab', 'Dom']}
                            months={['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro']}
                            previousTitle="Mês Anterior"
                            nextTitle="Próximo Mês"
                            todayBackgroundColor="#FFBE7E"
                            selectedDayColor="#ff7f00"
                            selectedDayTextColor="#fff"
                            scaleFactor={375}
                            disabledDatesTextStyle={{ color: '#cccccc' }}
                            textStyle={{
                                fontFamily: 'MuseoSans-300',
                                color: '#7b7b7b',
                            }}
                            onDateChange={(date, type) => { setDate(date), setModal(false) }}
                        />
                    </View>
                </View>
            </Modal>
        )
    }





    const RenderModalConfirmNames = () => {
        return (
            <Modal animationType="slide" transparent={true} visible={modalConfirm} onRequestClose={() => { }}>
                <View style={{ width: '90%', height: 250, alignSelf: 'center', marginTop: 250, borderColor: '#ff7f00', borderWidth: 1, borderRadius: 50, backgroundColor: 'rgba(255,255,255,0.9)' }}>
                    <Text style={[style.normalText, { padding: 30, color: '#a8a8a8' }]}>Confirme a ou retire o nome desejado para administrar os nomes na lista deste evento.</Text>
                    <View style={{ alignItems: 'center', justifyContent: 'space-around' }}>
                        {administratorCheck === '' &&
                            <>
                                <Icon name="check" size={50} color="#ff7f00" onPress={() => { handleSetAdministratorProfile(admProfileData, event, dispatch), setModalConfirm(false) }} />
                                <Text style={style.normalText}>Selecionar perfil</Text>
                            </>
                        }
                        {administratorCheck === false &&
                            <>
                                <Icon name="check" size={50} color="#ff7f00" onPress={() => { handleSetAdministratorProfile(admProfileData, event, dispatch), setModalConfirm(false) }} />
                                <Text style={style.normalText}>Selecionar perfil</Text>
                            </>
                        }
                        {administratorCheck === true &&
                            <>
                                <Icon name="delete" size={50} color="#ff7f00" onPress={() => { handleRemoveAdministratorProfileFromEvent(admProfileData, event, dispatch), setModalConfirm(false) }} />
                                <Text style={style.normalText}>Retirar perfil</Text>
                            </>
                        }
                    </View>
                    <Icon style={style.absoluteRight} name="close" size={30} color="#ff7f00" onPress={() => { setModalConfirm(false) }} />

                </View>
            </Modal>
        )
    }






    return (
        <View style={{ flex: 1 }}>
            <Header text={'_editando/Alterando'} title={event.name} onPress={() => { props.navigation.openDrawer() }} />
            <RenderModalConfirmNames />
            <LoadingNumeric percentage={photoProgress.progress} loading={photoProgress.loading} />
            <ModalDate />
            <View><Loading loading={loadingEvent} /></View>
            <ScrollView>

                <View style={{ marginLeft: '10%', marginRight: '10%', marginTop: 20 }}>
                    <Text style={style.headerText}>Alterar nome</Text>
                    <View style={{ flexDirection: 'row' }}>
                        <Text style={style.normalText}>Seu nome atual: </Text>
                        <Text style={[style.normalTextBold, { marginBottom: 10 }]}>{event.name}</Text>
                    </View>
                    <TextInput style={style.inputText} placeholder='Insira o novo nome' underlineColorAndroid='transparent' value={newName} onChangeText={t => setNewName(t)} />
                    <BarButton label={'Salvar'} width={150} onPress={() => { handleChangeEventName(event.id, event.owner, newName, dispatch, props.navigation), setNewName('') }} />
                </View>
                <View style={{ marginTop: 40, borderBottomWidth: 1, borderBottomColor: '#cccc', marginLeft: 10, marginRight: 10 }}></View>



                <View style={{ marginLeft: '10%', marginRight: '10%', marginTop: 40 }}>
                    <Text style={style.headerText}>Alterar descrição</Text>
                    <View style={{ flexDirection: 'row' }}>
                        <Text style={[style.normalText, { marginBottom: 10 }]}>Sua descrição atual: </Text>
                    </View>
                    <TextInput style={style.inputTextMultiLine} multiline={true} numberOfLines={3} placeholder='Descrição do evento' underlineColorAndroid='transparent' value={description} onChangeText={t => setDescription(t)} />
                    <BarButton label={'Salvar'} width={150} onPress={() => { handleChangeEventDescription(event.id, event.owner, description, dispatch, props.navigation), setDescription(description) }} />
                </View>
                <View style={{ marginTop: 40, borderBottomWidth: 1, borderBottomColor: '#cccc', marginLeft: 10, marginRight: 10 }}></View>



                <View style={{ marginLeft: '10%', marginRight: '10%', marginTop: 40 }}>
                    <Text style={style.headerText}>Alterar data</Text>
                    <View style={{ flexDirection: 'row' }}>
                        <Text style={style.normalText}>Sua data atual: </Text>
                        <Text style={[style.normalTextBold, { marginBottom: 10 }]}>{event.dateDescription}</Text>
                    </View>
                    <View style={{ flexDirection: 'row' }}>
                        <Text style={style.normalText}>A nova data será: </Text>
                        <Text style={[style.normalTextBold, { marginBottom: 10 }]}>{date ? dateDescription : 'Selecione uma nova data abaixo'}</Text>
                    </View>
                    <BarButton label={date ? 'Salvar' : 'Selecionar nova data'} width={date ? 150 : 300} onPress={date ? () => { handleChangeEventDate(event.id, event.owner, dateDescription, dateTimestamp, dispatch, props.navigation), setDate('') } : () => { setModal(true) }} />
                </View>
                <View style={{ marginTop: 40, borderBottomWidth: 1, borderBottomColor: '#cccc', marginLeft: 10, marginRight: 10 }}></View>



                <View style={{ marginLeft: '10%', marginRight: '10%', marginTop: 40 }}>
                    <Text style={style.headerText}>Alterar foto</Text>
                    <Text style={[style.normalText, { marginBottom: 10 }]}>Selecione a nova foto no botão "+" e aperte em "Salvar". Caso queira trocar a foto, aperte novamente o botão "-".</Text>
                    <View style={{ flexDirection: 'row', alignItems: 'center', marginBottom: 7 }}>
                        <Text style={style.normalText}>Sua foto atual: </Text>
                        {imagePickerResponse.uri != '' &&
                            <Image style={[style.photo, { marginLeft: 10, height: 100, width: 100 }]} source={imagePickerResponse.uri
                                ? { uri: imagePickerResponse.uri }
                                : { uri: event.photos[0].fileUrl }}></Image>
                        }
                        <Icon style={{ marginBottom: 60, marginLeft: -20 }} name={imagePickerResponse.uri ? 'minus-circle' : 'plus-circle'} size={40} color="#a8a8a8" onPress={imagePickerResponse.uri ? () => { dispatch({ type: 'IMAGEPICKER_RESPONSE', imagePickerResponse: '' }) } : () => { imagePickForEvent(dispatch) }} />
                    </View>
                    <BarButton label={'Salvar'} width={150} onPress={() => { handleChangeEventPhoto(event.id, event.owner, imagePickerResponse, event.photos, dispatch, props.navigation) }} />
                </View>
                <View style={{ marginTop: 40, borderBottomWidth: 1, borderBottomColor: '#cccc', marginLeft: 10, marginRight: 10 }}></View>


                <View style={{ marginLeft: '10%', marginRight: '10%', marginTop: 40 }}>
                    <Text style={style.headerText}>Alterar tipos de nome na lista</Text>
                    <Text style={style.normalText}>Seus tipos de lista atuais: </Text>
                </View>


                {event.list.map((item, index) => {
                    return (
                        <View key={index} style={[style.dateBoxDetailWithButtons, { flexDirection: 'row', alignItems: 'center' }]}>
                            <View>
                                <Text style={[style.titleTextBold, { marginLeft: 20 }]}>{item.listType}</Text>
                                <Text style={[style.normalText, { marginLeft: 20 }]}>{item.listTypeDescription}</Text>
                            </View>
                            <View style={{ flexDirection: 'row', position: 'absolute', right: 20 }}>
                                <Icon name="pencil" size={30} color="#b4b4b4" onPress={() => { props.navigation.navigate('Manage') }} />
                                <Icon style={{ marginLeft: 20 }} name="delete" size={30} color="#b4b4b4" onPress={() => { handleDeleteTypeOfList(item, event, dispatch) }} />
                            </View>
                        </View>
                    )
                })}
                <View style={{ flexDirection: 'row', alignItems: 'center', marginLeft: 20 }}>
                    <Icon name="plus-circle" size={40} color="#ff7f00" onPress={() => { setEnableListInput(!enableListInput) }} />
                    <Text style={[style.normalTextBold, { marginLeft: 5 }]}>Aperte no ícone para criar um tipo de lista</Text>
                </View>
                {enableListInput === true &&
                    <View style={{ marginLeft: '10%', marginRight: '10%', marginTop: 20 }}>
                        <TextInput style={style.inputText} autoCapitalize='words' placeholder='Tipo' underlineColorAndroid='transparent' value={listType} onChangeText={t => setListType(t)} />
                        <TextInput style={style.inputText} placeholder='Descrição' underlineColorAndroid='transparent' value={listTypeDescription} onChangeText={t => setListTypeDescription(t)} />
                        <BarButton label={'Salvar'} width={150} onPress={() => { handleAddTypeOfList(listType, listTypeDescription, event, dispatch, props.navigation), setEnableListInput(!enableListInput), setListType(''), setListTypeDescription('') }} />
                    </View>
                }
                <View style={{ marginTop: 40, borderBottomWidth: 1, borderBottomColor: '#cccc', marginLeft: 10, marginRight: 10 }}></View>



                <View style={{ marginLeft: '10%', marginRight: '10%', marginTop: 40, }}>
                    <Text style={style.headerText}>Alterar/Selecionar Perfil Administrativo</Text>
                    <Text style={style.normalText}>Seus Perfis Administrativos já selecionados para esse evento: </Text>
                    {event.administrators === true && event.administrators.map((item, index) => {
                        return (
                            <View key={index} style={{ flexDirection: 'row', alignItems: 'center', marginTop: 10 }}>
                                <Text style={[style.normalTextBold, { marginLeft: 0 }]}>{item.name}</Text>
                                <Icon style={{ marginLeft: 10 }} name="circle" size={10} color="#cccc" />
                                <Text style={[style.normalText, { marginLeft: 10 }]}>{item.email}</Text>
                            </View>
                        )
                    })}
                    <Text style={[style.normalText, { marginTop: 20 }]}>Seus Perfis Administrativos disponíveis: </Text>
                </View>
                {administrativeProfiles.map((item, index) => {
                    return (
                        <View key={index} style={style.dateBox}>
                            <Text style={[style.titleTextBold, { marginLeft: 20 }]}>{item.name}</Text>
                            <Icon style={{ marginLeft: 20 }} name="circle" size={15} color="#fff" />
                            <Text style={[style.normalText, { marginLeft: 20 }]}>{item.email}</Text>
                            <View style={{ backgroundColor: '#eeeeee', position: 'absolute', right: 20, justifyContent: 'center' }}>
                                <Text style={[style.normalText, {}]} onPress={() => { setAdmProfileData(item), setModalConfirm(true) }}>  Editar</Text>
                            </View>
                        </View>
                    )
                })}
                <View style={{ marginTop: 40, borderBottomWidth: 1, borderBottomColor: '#cccc', marginLeft: 10, marginRight: 10 }}></View>



                <View style={{ marginLeft: '10%', marginRight: '10%', marginTop: 40 }}>
                    <Text style={style.headerText}>Apagar evento</Text>
                    <View style={{ flexDirection: 'row' }}>
                        <Text style={style.normalText}>Nome do evento adual: </Text>
                        <Text style={[style.normalTextBold, { marginBottom: 10 }]}>{event.name}</Text>
                    </View>
                    <BarButton marginBottom={10} label={'Apagar'} width={150} onPress={() => { handleDeleteEvent(event.id, event.owner, event.photos, dispatch, props.navigation) }} />
                    <Text style={[style.normalText, { marginBottom: 60 }]}>Ao excluir um evento, você apaga todos os seus dados em nosso aplicativo e site e não será possivel recuperá-los depois.</Text>
                </View>
            </ScrollView>
            <Icon style={style.absoluteRight} name="keyboard-backspace" size={50} color="#b4b4b4" onPress={() => { props.navigation.navigate('Manage') }} />
        </View>
    )
}
export default EditEvent
