import React, { Component } from 'react';
import { Text, View, FlatList, TouchableOpacity } from 'react-native';
import BarButton from '../../components/barButton'
import { style } from '../../styles'
import Icon from 'react-native-vector-icons/MaterialCommunityIcons'
import { useSelector, useDispatch } from 'react-redux'
import Header from '../../components/header'


const AdministrativeProfiles = (props) => {
    
    const administrativeProfiles = useSelector(state => state.Reducer.administrativeProfiles)
    const userInfo = useSelector(state => state.Reducer.userInfo)

    return (
        <View style={{ flex: 1 }}>
            <Header text={'Perfis'} title={'_administrativos'} onPress={() => { props.navigation.openDrawer() }} />
            <View style={{ marginLeft: '10%', marginRight: '10%', marginTop: 20  }}>
						<Text style={style.headerText}>Cadastro de Perfil Administartivo</Text>
						<Text style={style.normalText}>Seja bem-vindo. Aqui você pode cadastrar uma pessoa que irá recepcionar as pessoas em seus eventos e também
																		gerenciar os nomes na lista enviados pelos usuários.</Text>
                                                                        <BarButton label={'Criar Perfil Administrativo'} marginTop={20} width={250} onPress={() => { props.navigation.navigate('SignUpAdministrative')}} />
					</View>
                    
                    <View style={{  marginLeft: '10%', marginRight: '10%', marginTop: 40, marginBottom: 20  }}>
						<Text style={style.headerText}>Perfis Cadastrados</Text>
						<Text style={style.normalText}>Abaixo você pode visualizar seus perfis já cadastrados. Você também pode editá-los ou apaga-los.</Text>
					</View>


            <FlatList
                data={administrativeProfiles}
                renderItem={({ item }) => <View style={{ flex: 1 }}>
                    <View style={style.dateBox}>
                        <Text style={[style.titleTextBold, { marginLeft: 20 }]}>{item.name}</Text>
                        <Icon style={{ marginLeft: 20 }} name="circle" size={15} color="#fff" />
                        <Text style={[style.normalText, { marginLeft: 20 }]}>{item.email}</Text>
                    </View>
                </View>}
                keyExtractor={(item) => item.id}
                showsVerticalScrollIndicator={false}
            />
            <Icon style={style.absoluteRight} name="keyboard-backspace" size={50} color="#b4b4b4" onPress={() => { props.navigation.navigate('Home') }} />

            
        </View>
    )
}
export default AdministrativeProfiles
