import React, { useState, useEffect, useCallback } from 'react';
import { View, TouchableOpacity, FlatList, Modal, Text } from 'react-native';
import RoundButtonEdit from '../../components/roundButtonEdit'
import RoundButtonConfirm from '../../components/roundButtonConfirm'

import { style } from '../../styles'
import Icon from 'react-native-vector-icons/MaterialCommunityIcons'
import { useSelector, useDispatch } from 'react-redux'
import Header from '../../components/header'
import { handleSetUserListType, handleSetUserConfirmationInEvent, handleRemoveUserConfirmationInEvent } from '../../functions/appFunctions'


const ListManage = (props) => {
    const dispatch = useDispatch()

    const userInfo = useSelector(state => state.Reducer.userInfo)
    const eventId = props.navigation.state.params.event
    const placeEventsForManage = useSelector(state => state.Reducer.placeEventsForManage)

    const event = placeEventsForManage.find( ({ id }) => id === eventId.id );

    const [selected, setSelected] = useState(new Map())
    const [modalEdit, setModalEdit] = useState(false)
    const [modalConfirm, setModalConfirm] = useState(false)

    const onSelect = useCallback((id) => {
        const newSelected = new Map(selected)
        newSelected.set(id, !selected.get(id))
        setSelected(newSelected);
    },
        [selected],
    )

    const selectedArray = []
    selected.forEach((value, key) => {
        selectedArray.push({
            key: key,
            value: value
        })
    })
    const selectedNames = selectedArray.filter(item => item.value == true)

    const RenderModalEditNames = () => {
        return (
            <Modal animationType="slide" transparent={true} visible={modalEdit} onRequestClose={() => { }}>
                <View style={{ width: '90%', height: 450, alignSelf: 'center', marginTop: 150, borderColor: '#ff7f00', borderWidth: 1, borderRadius: 50, backgroundColor: 'rgba(255,255,255,0.8)' }}>
                    <Text style={[style.normalText, { padding: 30, color: '#a8a8a8' }]}>Selecione o tipo desejado abaixo. Lembrando que a opção será aplicada à todos
                                                                                                                                            os normes que foram selecionados anteriormente:</Text>
                    <FlatList
                        data={event.list}
                        renderItem={({ item }) => (
                            <View style={{}}>
                                <TouchableOpacity style={style.dateBoxDetail} onPress={() => { handleSetUserListType(selectedNames, event, item, dispatch), setModalEdit(false) }}>
                                    <Text style={[style.titleTextBold, { marginLeft: 20 }]}>{item.listType}</Text>
                                    <Text style={[style.normalText, { marginLeft: 20 }]}>{item.listTypeDescription}</Text>
                                </TouchableOpacity>
                            </View>
                        )}
                        keyExtractor={item => item.listType}
                    />
                    <Icon style={{ position: 'absolute', left: 20, bottom: 20 }} name="close" size={50} color="#ff7f00" onPress={() => { setModalEdit(false) }} />
                </View>
            </Modal>
        )
    }


    const RenderModalConfirmNames = () => {
        return (
            <Modal animationType="slide" transparent={true} visible={modalConfirm} onRequestClose={() => { }}>
                <View style={{ width: '90%', height: 250, alignSelf: 'center', marginTop: 250, borderColor: '#ff7f00', borderWidth: 1, borderRadius: 50, backgroundColor: 'rgba(255,255,255,0.8)' }}>
                    <Text style={[style.normalText, { padding: 30, color: '#a8a8a8' }]}>{selectedNames.length > 1 ? 'Deseja confirmar a entrada das pessoas selecionadas no evento' :'Deseja confirmar a entrada da pessoa selecionada no evento'}</Text>
                    <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'space-around' }}>
                        <Icon style={{}} name="check" size={50} color="#ff7f00" onPress={() => { handleSetUserConfirmationInEvent(selectedNames, event, dispatch), setModalConfirm(false) }} />
                        <Icon style={{}} name="delete" size={50} color="#ff7f00" onPress={() => { handleRemoveUserConfirmationInEvent(selectedNames, event, dispatch), setModalConfirm(false) }} />
                    </View>
                    <Icon style={style.absoluteRight} name="close" size={30} color="#ff7f00" onPress={() => { setModalConfirm(false) }} />

                </View>
            </Modal>
        )
    }

    const FlatListItem = ({ id, userName, listType, listTypeDescription, isPresent, selected, onSelect }) => {
        return (
            <TouchableOpacity onPress={() => onSelect(id)} style={[style.dateBoxDetail, { backgroundColor: selected ? '#eeee' : '#eeeeee' }]}>

                {isPresent &&
                    <Icon style={{ position: 'absolute', left: 20 }} name="check" size={30} color={'#008000'} />
                }
                <View style={{ marginLeft: isPresent ? 40 : 0}}>
                <Text style={[style.titleText, { marginLeft: 20, marginBottom: 5, color: '#a8a8a8' }]}>{userName}</Text>
                <View style={{ flexDirection: 'row' }}>
                    <Text style={[style.normalText, { marginLeft: 20, color: '#a8a8a8' }]}>Tipo:</Text>
                    <Text style={[style.normalTextBold, { marginLeft: 3, color: '#a8a8a8' }]}>{listType}</Text>
                </View>
                </View>
                <Icon style={{ position: 'absolute', right: 25 }} name="circle" size={30} color={selected ? '#fff' : '#fff'} />
                <Icon style={{ position: 'absolute', right: 30 }} name="circle" size={20} color={selected ? '#a8a8a8' : '#fff'} />
            </TouchableOpacity>
        )
    }

    return (
        <View style={{ flex: 1 }}>
            <Header text={'Bem vindo ao'} title={'_gerenciamento de nomes'} onPress={() => { props.navigation.openDrawer() }} />
            <Text style={[style.normalText, { padding: 20, }]}>Caso você queira, você pode atribuir os tipos de lista criados por você na hora de criar eventos
            aos nomes da lista. Basta selecionar um ou mais nomes e selecionar o botão editar. Se você não
                                                                                            editar, o nome permanece sem atributo.</Text>
            <RenderModalEditNames />
            <RenderModalConfirmNames />
            <FlatList
                data={event.listNames.sort((a, b) => a.name.localeCompare(b.name))}
                renderItem={({ item }) => (
                    <FlatListItem
                        id={item.uid}
                        userName={item.name}
                        listType={item.listType}
                        listTypeDescription={item.listTypeDescription}
                        isPresent={item.werePresent}
                        selected={!!selected.get(item.uid)}
                        onSelect={onSelect}
                    />
                )}
                keyExtractor={item => item.uid}
                extraData={selected}
            />
            <View style={{ flexDirection: 'row', marginBottom: 20, }}>
                {selectedNames.some(item => item.value == true) &&
                    <RoundButtonEdit marginLeft={20} onPress={() => { setModalEdit(true) }} />
                }
                {selectedNames.some(item => item.value == true) &&
                    <RoundButtonConfirm marginLeft={20} onPress={() => { setModalConfirm(true) }} />
                }
            </View>

            <Icon style={style.absoluteRight} name="keyboard-backspace" size={50} color="#b4b4b4" onPress={() => { userInfo.userType === 'Administrative' ? props.navigation.navigate('HomeAdministrative') : props.navigation.navigate('Manage'), setSelected(new Map()) }} />
        </View>
    )
}

export default ListManage
