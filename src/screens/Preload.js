import React, { useEffect } from 'react'
import { Text, View, Image } from 'react-native';
import { style } from '../styles'
import { preloadCheck } from '../functions/authFunctions'
import firebase from '../services/firebase'

import { useSelector, useDispatch } from 'react-redux'


const Preload = (props) => {
    const dispatch = useDispatch()
    const loginStatus = useSelector(state => state.Reducer.loginStatus)



    useEffect(() => {
        //firebase.auth().signOut()
        preloadCheck(dispatch, props.navigation)

    }, []);

    const ImageLoad = () => {
        return (
            <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
                <Image
                style={{width: 200, height: 200}}
                    source={require('../../assets/images/logo.png')}
                />
            </View>
        )
    }


    return (
        <ImageLoad />
    )
}

export default Preload