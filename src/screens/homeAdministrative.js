import React, { Component } from 'react';
import { Text, View, FlatList, TouchableOpacity } from 'react-native';
import BarButton from '../components/barButton'
import { style } from '../styles'
import Icon from 'react-native-vector-icons/MaterialCommunityIcons'
import { useSelector, useDispatch } from 'react-redux'
import Header from '../components/header'


const HomeAdministrative = (props) => {

    const placeEventsForManage = useSelector(state => state.Reducer.placeEventsForManage)
    const userInfo = useSelector(state => state.Reducer.userInfo)

    const filteredPlaceEventsForManage = placeEventsForManage.filter(({administrators})=> administrators.some(({id})=> id === userInfo.uid))

    return (
        <View style={{ flex: 1 }}>
            <Header text={'Bem vindo ao'} title={'_gerenciamento de eventos'} onPress={() => { props.navigation.openDrawer() }} />
            <Text style={[style.titleText, { marginTop: 20, marginLeft: '10%' , marginRight: '10%' }]}>Você tem eventos para gerenciar</Text>

            <FlatList
                data={filteredPlaceEventsForManage}
                renderItem={({ item }) => <View style={{ flex: 1 }}>
                    <View style={style.dateBox}>
                        <Text style={[style.titleTextBold, { marginLeft: 20 }]}>{item.name}</Text>
                        <Icon style={{ marginLeft: 20 }} name="circle" size={15} color="#fff" />
                        <Text style={[style.normalText, { marginLeft: 20 }]}>{item.dateDescription}</Text>
                    </View>

                    <View style={style.container}>
                        <Text style={[style.normalTextBold, { marginTop: 10 }]}>Descrição do evento:</Text>
                        <Text style={[style.normalText, { marginTop: 10, marginBottom: 10 }]}>{item.description}</Text>
                        <BarButton label={'Gerenciar nomes da lista'} marginBottom={10} width={250} onPress={() => {props.navigation.navigate('ListManage', {event: item})}} />
                    </View>

                </View>}
                keyExtractor={(item) => item.id}
                showsVerticalScrollIndicator={false}
            />
            <Icon style={style.absoluteRight} name="keyboard-backspace" size={50} color="#b4b4b4" onPress={() => { props.navigation.navigate('Home') }} />


        </View>
    )
}
export default HomeAdministrative
