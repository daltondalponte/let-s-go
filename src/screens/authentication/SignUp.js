import React, { useState, useEffect } from 'react';
import { Text, View, KeyboardAvoidingView, Platform, TextInput, Button, Modal, TouchableHighlight } from 'react-native';
import { style } from '../../styles'
import RoundButton from '../../components/roundButton'
import { TextInputMask } from 'react-native-masked-text'
import { useSelector, useDispatch } from 'react-redux'
import Loading from '../../components/loading'
import { handleSignUp } from '../../functions/authFunctions'



const SignUp = (props) => {
	const dispatch = useDispatch()

	const loadingAuth = useSelector(state => state.Reducer.loadingAuth)


	const [name, setName] = useState('')
	const [email, setEmail] = useState('')
	const [cpf, setCpf] = useState('')
	const [password, setPassword] = useState('')
	const [confirmPassword, setConfirmPassword] = useState('')

	return (
		<KeyboardAvoidingView style={{ flex: 1 }} behavior={(Platform.OS === 'ios') ? "padding" : null} enabled>
			<View><Loading loading={loadingAuth} /></View>
			<View style={style.container}>
				<View style={{ marginBottom: 20 }}>
					<Text style={style.headerText}>Cadastro</Text>
					<Text style={style.normalText}>Seja bem-vindo</Text>
				</View>
				<View style={{ marginBottom: 20 }}>
					<TextInput style={style.inputText} placeholder='Nome' underlineColorAndroid='transparent' value={name} onChangeText={t => setName(t)} />
					<TextInput style={style.inputText} autoCapitalize='none' keyboardType="email-address" placeholder='Email' underlineColorAndroid='transparent' value={email} onChangeText={t => setEmail(t)} />
					<TextInputMask style={style.inputText} placeholder='CPF' underlineColorAndroid='transparent' type={'cpf'} value={cpf} onChangeText={t => setCpf(t)} />
					<TextInput style={style.inputText} secureTextEntry placeholder='Senha' underlineColorAndroid='transparent' value={password} onChangeText={t => setPassword(t)} />
					<TextInput style={style.inputText} secureTextEntry placeholder='Confirmar Senha' underlineColorAndroid='transparent' value={confirmPassword} onChangeText={t => setConfirmPassword(t)} />
				</View>
				<Text style={[style.normalText, { marginBottom: 15 }]} onPress={() => props.navigation.navigate('SignIn')}>Já tenho cadastro, voltar para o Login</Text>
			</View>
			<RoundButton onPress={() => {
				if (email && email && cpf && password && confirmPassword) {
					if (password == confirmPassword) {
						handleSignUp(name, email, cpf, password, dispatch, props.navigation)
					} else {
						alert('Ops, sua senha e sua confirmação de senha estão diferentes')
					}
				} else {
					alert('Por favor, preencha todos os campos corretamente')
				}
			}} />
		</KeyboardAvoidingView>
	)
}

export default SignUp