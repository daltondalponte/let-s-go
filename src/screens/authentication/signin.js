import React, { useState, useEffect } from 'react';
import { Text, View, KeyboardAvoidingView, Platform, TextInput, TouchableOpacity } from 'react-native';
import { style } from '../../styles'
import RoundButton from '../../components/roundButton'
import Loading from '../../components/loading'
import Icon from 'react-native-vector-icons/FontAwesome'
import { useSelector, useDispatch } from 'react-redux'
import { handleSignIn } from '../../functions/authFunctions'

const SignIn = (props) => {

	const dispatch = useDispatch()
	const loginStatus = useSelector(state => state.Reducer.loginStatus)
	const loadingAuth = useSelector(state => state.Reducer.loadingAuth)


	const [email, setEmail] = useState('')
	const [password, setPassword] = useState('')

	return (
		<KeyboardAvoidingView style={{ flex: 1 }} behavior={(Platform.OS === 'ios') ? "padding" : null} enabled>
			<View><Loading loading={loadingAuth} /></View>
			<View style={style.container}>
				<View style={{ marginBottom: 20 }}>
					<Text style={style.headerText}>Login</Text>
					<Text style={style.normalText}></Text>
				</View>
				<View style={{ marginBottom: 20 }}>
					<TouchableOpacity style={[style.inputText, { flexDirection: 'row', alignItems: 'center' }]}>
						<Icon name="facebook" size={30} color="#114485" />
						<Text style={style.normalText}>Entrar com Facebook</Text>
					</TouchableOpacity>
					<TouchableOpacity style={[style.inputText, { flexDirection: 'row', alignItems: 'center' }]}>
						<Icon name="google-plus" size={30} color="#FF1200" />
						<Text style={style.normalText}>Entrar com Google</Text>
					</TouchableOpacity>
				</View>
				<View style={{ marginBottom: 20 }}>
					<TextInput style={style.inputText} autoCapitalize='none' keyboardType="email-address" placeholder='Email' underlineColorAndroid='transparent' value={email} onChangeText={t => setEmail(t)} />
					<TextInput style={style.inputText} secureTextEntry placeholder='Senha' underlineColorAndroid='transparent' value={password} onChangeText={t => setPassword(t)} />
				</View>
				<Text style={[style.normalText, { marginBottom: 15 }]} onPress={() => props.navigation.navigate('ForgotPassword')}>Esqueceu sua senha?</Text>
				<Text style={[style.normalText, { marginBottom: 15 }]} onPress={() => props.navigation.navigate('SignUp')}>Não tenho cadastro</Text>
				<Text style={[style.normalText, { marginBottom: 15 }]} onPress={() => props.navigation.navigate('SignUpBusiness')}>Cadastro jurídico</Text>
			</View>
			<RoundButton onPress={() => {
				if (email && password) {
					handleSignIn(email, password, dispatch, props.navigation)
				} else {
					alert('Por favor, digite seu email e senha corretamente')
				}
			}} />
		</KeyboardAvoidingView>
	)
}


export default SignIn