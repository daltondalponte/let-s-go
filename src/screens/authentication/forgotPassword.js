import React, { useState } from 'react';
import { Text, View, KeyboardAvoidingView, Platform, TextInput } from 'react-native';
import { style } from '../../styles'
import RoundButton from '../../components/roundButton'


const ForgotPassword = (props) => {

  const [email, setEmail] = useState('')
  
  return (
    <KeyboardAvoidingView style={{flex: 1}} behavior={(Platform.OS === 'ios')? "padding" : null} enabled>
        <View style={style.container}>
			<View style={{marginBottom:20}}>
				<Text style={style.headerText}>Esqueceu sua senha?</Text>
				<Text style={style.normalText}>Siga as instruções para recuperar sua senha!</Text>
                <Text style={[style.normalText, {marginBottom: 15}]}>Digite seu email e nós enviaremos sua senha.</Text>
			</View>
			<View style={{marginBottom:20}}>
				<TextInput style={style.inputText} autoCapitalize='none' keyboardType="email-address" placeholder='Email' underlineColorAndroid='transparent'  value={email} onChangeText={t=>setEmail(t)}/>
			</View>
                <Text style={[style.normalText, {marginBottom: 15}]}  onPress={() => props.navigation.navigate('SignIn')}>
                    Voltar
				</Text>
		</View>
        	<RoundButton/>
	</KeyboardAvoidingView>
  )
}

export default ForgotPassword
