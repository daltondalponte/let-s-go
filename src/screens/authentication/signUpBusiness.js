import React, { useState, useEffect } from 'react';
import { Text, View, KeyboardAvoidingView, Platform, TextInput, Modal } from 'react-native';
import { style } from '../../styles'
import RoundButton from '../../components/roundButton'
import { TextInputMask } from 'react-native-masked-text'
import AddressAutocomplete from '../../components/addressAutocomplete'
import BarButton from '../../components/barButton'
import Icon from 'react-native-vector-icons/MaterialCommunityIcons'
import { useSelector, useDispatch } from 'react-redux'
import Loading from '../../components/loading'
import { handleSignUpBusiness } from '../../functions/authFunctions'



const SignUpBusiness = (props) => {
	const dispatch = useDispatch()
	const tmp_address = useSelector(state => state.Reducer.tmp_address)

	const [name, setName] = useState('')
	const [email, setEmail] = useState('')
	const [cnpj, setCnpj] = useState('')
	const [password, setPassword] = useState('')
	const [confirmPassword, setConfirmPassword] = useState('')
	const [modal, setModal] = useState(false)

	const loadingAuth = useSelector(state => state.Reducer.loadingAuth)


	return (
		<KeyboardAvoidingView style={{ flex: 1 }} behavior={(Platform.OS === 'ios') ? "padding" : null} enabled>
			<View><Loading loading={loadingAuth} /></View>
			<View style={style.container}>
				<View style={{ marginBottom: 20 }}>
					<Text style={style.headerText}>Cadastro Jurídico</Text>
					<Text style={style.normalText}>Seja bem-vindo</Text>
				</View>

				<View style={{ marginBottom: 20 }}>
					<TextInput style={style.inputText} placeholder='Nome' underlineColorAndroid='transparent' value={name} onChangeText={t => setName(t)} />
					<TextInput style={style.inputText} autoCapitalize='none' keyboardType="email-address" placeholder='Email' underlineColorAndroid='transparent' value={email} onChangeText={t => setEmail(t)} />
					<TextInputMask style={style.inputText} placeholder='CNPJ' underlineColorAndroid='transparent' type={'cnpj'} value={cnpj} onChangeText={t => setCnpj(t)} />
					<BarButton label={tmp_address ? 'Alterar endereço' : 'Adicione seu endereço'} onPress={() => { setModal(true) }} />
					<Text style={[style.normalText, { marginTop: 15, marginBottom: 10 }]}>Endereço selecionado: {tmp_address.formatted_address}</Text>
					<Modal
						animationType="slide"
						transparent={true}
						visible={modal}
						onRequestClose={() => { }}>
						<View style={{
							marginTop: '10%',
							padding: 10,
							alignSelf: 'center',
							width: '90%',
							height: '90%',
							borderColor: '#ff7f00',
							borderWidth: 1,
							borderRadius: 25,
							backgroundColor: 'rgba(255,255,255,0.9)',
						}}>
							<AddressAutocomplete />
							<Text style={[style.titleText, { padding: 20 }]}>Endereço selecionado: {tmp_address.formatted_address}</Text>

							<Text style={[style.normalText, { padding: 20 }]}>Busque seu endereço. Depois, selecione seu endereço no resultado da busca. Então confirme no botão abaixo.</Text>

							<Icon style={{ marginLeft: 20 }} name="check" size={50} color="#ff7f00" onPress={() => { setModal(false) }} />

						</View>
					</Modal>
					<TextInput style={style.inputText} secureTextEntry placeholder='Senha' underlineColorAndroid='transparent' value={password} onChangeText={t => setPassword(t)} />
					<TextInput style={style.inputText} secureTextEntry placeholder='Confirmar Senha' underlineColorAndroid='transparent' value={confirmPassword} onChangeText={t => setConfirmPassword(t)} /></View>
				<Text style={[style.normalText, { marginBottom: 15 }]} onPress={() => props.navigation.navigate('SignIn')}>Já tenho cadastro, voltar para o Login</Text>
			</View>
			<RoundButton onPress={() => {
				if (email && email && cnpj && password && confirmPassword) {
					if (password == confirmPassword) {
						handleSignUpBusiness(name, email, cnpj, password, tmp_address.geometry.location, tmp_address.formatted_address, dispatch, props.navigation)
					} else {
						alert('Ops, sua senha e sua confirmação de senha estão diferentes')
					}
				} else {
					alert('Por favor, preencha todos os campos corretamente')
				}
			}} />
		</KeyboardAvoidingView>
	)
}

export default SignUpBusiness
