import React, { useState, useEffect } from 'react';
import { Text, View, KeyboardAvoidingView, Platform, TextInput, Button, Modal, ScrollView } from 'react-native';
import { style } from '../../styles'
import RoundButton from '../../components/roundButton'
import { TextInputMask } from 'react-native-masked-text'
import { useSelector, useDispatch } from 'react-redux'
import Loading from '../../components/loading'
import { handleSignUpAdministrative } from '../../functions/authFunctions'
import Icon from 'react-native-vector-icons/MaterialCommunityIcons'




const SignUpAdministrative = (props) => {
	const dispatch = useDispatch()

	const loadingAuth = useSelector(state => state.Reducer.loadingAuth)
    const userInfo = useSelector(state => state.Reducer.userInfo)


	const [name, setName] = useState('')
	const [email, setEmail] = useState('')
	const [cpf, setCpf] = useState('')
	const [password, setPassword] = useState('')
	const [confirmPassword, setConfirmPassword] = useState('')

	return (
		<View style={{flex: 1}}>
			<Header text={'olá,'} title={userInfo.name} onPress={() => { props.navigation.openDrawer() }} />
			<KeyboardAvoidingView style={{ flex: 1 }} behavior={(Platform.OS === 'ios') ? "padding" : null} enabled>
				<View><Loading loading={loadingAuth} /></View>
				<ScrollView style={{marginLeft: '10%', marginRight: '10%'}} showsVerticalScrollIndicator={false}>
					<View style={{ marginBottom: 20, marginTop: 50 }}>
						<Text style={style.headerText}>Cadastro de Perfil Administartivo</Text>
						<Text style={style.normalText}>Seja bem-vindo. Aqui você pode cadastrar uma pessoa que irá recepcionar as pessoas em seus eventos e também
																		gerenciar os nomes na lista enviados pelos usuários.</Text>
					</View>
					<View style={{ marginBottom: 20 }}>
						<TextInput style={style.inputText} placeholder='Nome' underlineColorAndroid='transparent' value={name} onChangeText={t => setName(t)} />
						<TextInput style={style.inputText} autoCapitalize='none' keyboardType="email-address" placeholder='Email' underlineColorAndroid='transparent' value={email} onChangeText={t => setEmail(t)} />
						<TextInputMask style={style.inputText} placeholder='CPF' underlineColorAndroid='transparent' type={'cpf'} value={cpf} onChangeText={t => setCpf(t)} />
						<TextInput style={style.inputText} secureTextEntry placeholder='Senha' underlineColorAndroid='transparent' value={password} onChangeText={t => setPassword(t)} />
						<TextInput style={style.inputText} secureTextEntry placeholder='Confirmar Senha' underlineColorAndroid='transparent' value={confirmPassword} onChangeText={t => setConfirmPassword(t)} />
					</View>
					<View style={{ marginBottom: 20, marginTop: 20 }}>
						<Text style={style.normalTextBold}>Por questões de segurança, assim que você criar um Perfil Administrativo, você será redirecionado para a área de login. Basta fazer o login novamente
																		e continuar usando o app.</Text>
					</View>
				</ScrollView>
				<RoundButton onPress={() => {
					if (email && email && cpf && password && confirmPassword) {
						if (password == confirmPassword) {
							handleSignUpAdministrative(name, email, cpf, password, userInfo.uid, dispatch, props.navigation)
						} else {
							alert('Ops, sua senha e sua confirmação de senha estão diferentes')
						}
					} else {
						alert('Por favor, preencha todos os campos corretamente')
					}
				}} />
			</KeyboardAvoidingView>
			<Icon style={style.absoluteLeft} name="keyboard-backspace" size={50} color="#b4b4b4" onPress={() => { props.navigation.navigate('AdministrativeProfiles') }} />
		</View>

	)
}

export default SignUpAdministrative