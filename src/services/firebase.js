import firebase from '@react-native-firebase/app';
import '@react-native-firebase/auth';
import '@react-native-firebase/firestore';
import '@react-native-firebase/storage'



var firebaseConfig = {
    apiKey: "AIzaSyBzwQrdFLxsNTvNO_TtX2jws76dPPk5dMM",
    authDomain: "lets-go-c35be.firebaseapp.com",
    databaseURL: "https://lets-go-c35be.firebaseio.com",
    projectId: "lets-go-c35be",
    storageBucket: "lets-go-c35be.appspot.com",
    messagingSenderId: "206652903629",
    appId: "1:206652903629:web:e3ffdf5182f5b65809d984",
    measurementId: "G-6LMRQEN56B"
  };

  if (!firebase.apps.length) {
    firebase.initializeApp(firebaseConfig)
}

  export default firebase;
  
